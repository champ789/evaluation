<?php
final class Cache
{
    public $site;
    protected $memcache;
    public $cacheOff = false;

    public function __construct()
    {
        $this->site = SITE;
        $this->memcache = new Memcache;
        $this->memcache->connect('localhost', 11211);
        // $this->memcache = new Memcached();
        // $this->memcache->addServer('localhost', 11211);
    }

    public function set($key, $value, $time = 0)
    {
        if (!$array = $this->memcache->get($this->site . '-keys')) {
            $array = array();
        }

        if (!isset($array[$key])) {
            $array[$key] = true;
            $this->memcache->set($this->site . '-keys', $array);
        }

        $key = $this->site . '-' . $key;
        $this->memcache->set($key, $value, $time);
    }

    public function get($key)
    {
        if ($this->cacheOff) {
            return false;
        }
        $key = $this->site . '-' . $key;
        $return = $this->memcache->get($key);
        return $return;
    }

    public function keys()
    {
        $keys = $this->memcache->get($this->site . '-keys');
        return $keys;
    }

    public function delete($key)
    {
        $key = $this->site . '-' . $key;
        $this->memcache->delete($key);
    }

    public function flush()
    {
        $keys = $this->keys();
        foreach ($keys as $key => $val) {
            $this->delete($key);
        }
        $this->memcache->delete($this->site . '-keys');
    }
}
