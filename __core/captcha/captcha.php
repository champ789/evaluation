<?php
header('Content-type: image/png');
header("Cache-Control: must-revalidate");
header("Expires: " . gmdate("D, d M Y H:i:s", time() + (60 * 60 * 24 * 3)) . " GMT");

require_once('../../_constant.php');
require_once('../session.php');
$session = new Session(SITE);

$im = imagecreate(50, 20);
$bg = imagecolorallocate($im, 0, 0, 0);
$textcolor = imagecolorallocate($im, 210, 210, 210);
imagecolortransparent($im, $bg);
$random = rand(10000,99999);
$random = str_replace(array('0','1'),array('8','9'),$random);

if(!isset($session->captcha))
{
	$session->captcha = array();
}

if(count($session->captcha)>5)
{
	$session->captcha = array_slice($session->captcha, -10, 10);
}

$session->captcha[] = $random;

$font = 'fonts/captcha-komtit.ttf';
imagettftext($im, 14, 0, 0, 18, $textcolor, $font, $random); 
// $background = @imagecreatefromjpeg('images/captcha.jpg');
$background = @imagecreatefromgif('images/captcha.gif');
$randx = -5;
$randy = 0;
imagecopy($background, $im, 0, 0, $randx, $randy, 50, 20);
imagepng($background);
imagedestroy($im);
imagedestroy($background);
?>