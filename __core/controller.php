<?php
class Controller {
	protected $template;
	protected $html = array();
	protected $data = array();
	protected $token = array();
	protected $output;
	
	public function __get($key)
	{
		$this->$key = Registry::get($key);
		return $this->$key;
	}
	
	public function action($action,$args=array(),$method='index')
	{
		$file   = ROOT_PATH .'_controller/' . str_replace('../', '', $action . '.php');
		$class  = 'Controller' . preg_replace('/[^a-zA-Z0-9]/', '', $action);
		
		require_once($file);
		
		$controller = new $class();
		$controller->data = $args;
		$controller->token = $args;
		$controller->template = $action;
		return $controller->$method();
	}
	
	protected function render($html = FALSE)
	{
		$this->output = $this->fetch($this->template);
		
		if($html)
		{
			$file = ROOT_PATH .'_controller/__html.php';
			$class = 'ControllerHtml';
			
			require_once($file);
			
			$controller = new $class();
			
			$controller->data['_wrapper'] = $this->output;
			
			$controller->token = $this->html;
			$controller->template = '__html';
			$controller->index();
			
			echo $controller->output;
			
			unset($controller->data);
			unset($controller->token);
			
			unset($this->data);
			unset($this->token);
			unset($this->html);
		}
		else
		{
			return $this->output;
		}
	}
	
	protected function fetch($filename)
	{
		$file = ROOT_PATH .'_template/'. $filename .'.php';
		
		extract($this->data);
		
		foreach($this->token as $key => $val)
		{
			${$key} = $val;
		}
		
		ob_start();
		require($file);
		$content = ob_get_contents();
		ob_end_clean();
		
		unset($this->data);
		unset($this->token);
		
		return $content;
	}
}
?>