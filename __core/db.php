<?php
final class DB
{
	public $prefix;
    protected $mysqli;
    
    public function __construct()
    {
        include(dirname(__FILE__).'/secret.php');
        $mysqli = new mysqli($secret['server'],$secret['user'],$secret['password'],$secret['database']);
        $mysqli->set_charset("utf8");
        $this->mysqli = $mysqli;
		$this->prefix = DB_PREFIX;
    }
    
    public function query($query)
    {
		$query = str_replace('{{PREFIX}}',$this->prefix,$query);
        $return = $this->mysqli->query($query);
        return $return;
    }
    
    public function select($query)
    {
		$query = str_replace('{{PREFIX}}',$this->prefix,$query);
        $return = $this->mysqli->query($query);
        return $return;
    }
    
    public function insert($query)
    {
		$query = str_replace('{{PREFIX}}',$this->prefix,$query);
        $return = $this->mysqli->query($query);
        return $return;
    }
    
    public function update($query)
    {
		$query = str_replace('{{PREFIX}}',$this->prefix,$query);
        $return = $this->mysqli->query($query);
        return $return;
    }
    
    public function fetch($query)
    {
        $return = $query->fetch_assoc();
        return $return;
    }
    
    public function numrow($query)
    {
        $return = $query->num_rows;
        return $return;
    }    
    
    public function counts($query)
    {
        return $this->numrow($query);
    }
    
    public function escape($query)
    {
        $return = $this->mysqli->real_escape_string($query);
        return $return;
    }
    
    public function nextid()
    {
        $return = $this->mysqli->insert_id;
        return $return;
    }
    
    public function freeresult($query)
    {
        $query->free();
    }
    
    public function close()
    {
        $this->mysqli->close();
    }
}
?>