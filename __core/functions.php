<?php
final class Functions
{
    public $custom = array();

    public function isMobile()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
    public function __get($key)
    {
        $this->$key = Registry::get($key);
        return $this->$key;
    }

    public function resize($images, $size = array(800, 600), $adv = false, $new_images = false)
    {
        require_once CORE_PATH . "gd/ThumbLib.inc.php";
        if (!$s = getimagesize($images)) {
            return false;
        }
        $return_type = true;
        if (!$new_images) {
            $return_type = false;
            $new_images = ROOT_PATH . md5($_SERVER['REQUEST_TIME'] . rand(11111, 99999)) . '.jpg';
        }
        $width = $size[0] ? $size[0] : $s[0];
        $height = $size[1] ? $size[1] : $s[1];
        $image_thumb = PhpThumbFactory::create($images);
        if ($adv) {
            $image_thumb->adaptiveResize($width, $height)->save($new_images);
        } else {
            $image_thumb->resize($width, $height)->save($new_images);
        }
        if ($return_type) {
            return;
        }
        $content = file_get_contents($new_images);
        @unlink($new_images);
        return $content;
    }

    public function get_time($Timestamp, $Format = "d F H:i", $Language = "th", $TimeText = true)
    {
        $SuffixTime = array(
            "time" => array(
                "Seconds" => " วินาทีที่แล้ว",
                "Minutes" => " นาทีที่แล้ว",
                "Hours" => " ชั่วโมงที่แล้ว",
            ),
            "day" => array(
                "Yesterday" => "เมื่อวาน เวลา ",
                "Monday" => "วันจันทร์ เวลา ",
                "Tuesday" => "วันอังคาร เวลา ",
                "Wednesday" => "วันพุธ เวลา ",
                "Thursday" => "วันพฤหัสบดี เวลา ",
                "Friday" => "วันศุกร์ เวลา ",
                "Saturday" => " วันวันเสาร์ เวลา ",
                "Sunday" => "วันอาทิตย์ เวลา ",
            ),
        );

        $DateThai = array(
            // Day
            "l" => array( // Full day
                "Monday" => "วันจันทร์",
                "Tuesday" => "วันอังคาร",
                "Wednesday" => "วันพุธ",
                "Thursday" => "วันพฤหัสบดี",
                "Friday" => "วันศุกร์",
                "Saturday" => "วันวันเสาร์",
                "Sunday" => "วันอาทิตย์",
            ),
            "D" => array( // Abbreviated day
                "Monday" => "จันทร์",
                "Tuesday" => "อังคาร",
                "Wednesday" => "พุธ",
                "Thursday" => "พฤหัส",
                "Friday" => "ศุกร์",
                "Saturday" => "วันเสาร์",
                "Sunday" => "อาทิตย์",
            ),

            // Month
            "F" => array( // Full month
                "January" => "มกราคม  เวลา",
                "February" => "กุมภาพันธ์ เวลา",
                "March" => "มีนาคม เวลา",
                "April" => "เมษายน เวลา",
                "May" => "พฤษภาคม เวลา",
                "June" => "มิถุนายน เวลา",
                "July" => "กรกฎาคม เวลา",
                "August" => "สิงหาคม เวลา",
                "September" => "กันยายน เวลา",
                "October" => "ตุลาคม เวลา",
                "November" => "พฤศจิกายน เวลา",
                "December" => "ธันวาคม เวลา",
            ),
            "M" => array( // Abbreviated month
                "January" => "ม.ค.",
                "February" => "ก.พ.",
                "March" => "มี.ค.",
                "April" => "เม.ย.",
                "May" => "พ.ค.",
                "June" => "มิ.ย.",
                "July" => "ก.ค.",
                "August" => "ส.ค.",
                "September" => "ก.ย.",
                "October" => "ต.ค.",
                "November" => "พ.ย.",
                "December" => "ธ.ค.",
            ),
        );
        if (date("Ymd", $Timestamp) >= date("Ymd", (time() - 86400)) && $TimeText) // Less than 3 days.
        {
            $TimeStampAgo = (time() - $Timestamp);

            if (($TimeStampAgo < 86400)) // Less than 1 day.
            {

                $TimeDay = "time"; // Use array time

                if ($TimeStampAgo < 60) // Less than 1 minute.
                {
                    $Return = (time() - $Timestamp);
                    $Values = "Seconds";
                } else if ($TimeStampAgo < 3600) // Less than 1 hour.
                {
                    $Return = floor((time() - $Timestamp) / 60);
                    $Values = "Minutes";
                } else // Less than 1 day.
                {
                    $Return = floor((time() - $Timestamp) / 3600);
                    $Values = "Hours";
                }
            } else if ($TimeStampAgo < 172800) // Less than 2 day.
            {
                $Return = date("H:i", $Timestamp);
                $TimeDay = "day";
                $Values = "Yesterday";
            }
            if ($TimeDay == "time") {
                $Return .= $SuffixTime[$TimeDay][$Values];
            } else if ($TimeDay == "day") {
                $Return = $SuffixTime[$TimeDay][$Values] . $Return . " น.";
            }

            return $Return;
        } else {
            if ($Language == "en") {
                return date($Format, $Timestamp);
            } else if ($Language == "th") {
                $StrCache = '';
                $Format = str_replace("l", "|1|", $Format);
                $Format = str_replace("D", "|2|", $Format);
                $Format = str_replace("F", "|3|", $Format);
                $Format = str_replace("M", "|4|", $Format);
                $Format = str_replace("y", "|x|", $Format);
                $Format = str_replace("Y", "|X|", $Format);

                $DateCache = date($Format, $Timestamp);

                $AR1 = array("", "l", "D", "F", "M");
                $AR2 = array("", "l", "l", "F", "F");

                for ($i = 1; $i <= 4; $i++) {
                    if (strstr($DateCache, "|" . $i . "|")) {
                        //$Return .= $i;

                        $split = explode("|" . $i . "|", $DateCache);
                        for ($j = 0; $j < count($split) - 1; $j++) {
                            $StrCache .= $split[$j];
                            $StrCache .= $DateThai[$AR1[$i]][date($AR2[$i], $Timestamp)];
                        }
                        $StrCache .= $split[count($split) - 1];
                        $DateCache = $StrCache;
                        $StrCache = "";
                        empty($split);
                    }
                }

                if (strstr($DateCache, "|x|")) {

                    $split = explode("|x|", $DateCache);

                    for ($i = 0; $i < count($split) - 1; $i++) {
                        $StrCache .= $split[$i];
                        $StrCache .= substr((date("Y", $Timestamp) + 543), -2);
                    }
                    $StrCache .= $split[count($split) - 1];
                    $DateCache = $StrCache;
                    $StrCache = "";
                    empty($split);
                }

                if (strstr($DateCache, "|X|")) {

                    $split = explode("|X|", $DateCache);

                    for ($i = 0; $i < count($split) - 1; $i++) {
                        $StrCache .= $split[$i];
                        $StrCache .= (date("Y", $Timestamp) + 543);
                    }
                    $StrCache .= $split[count($split) - 1];
                    $DateCache = $StrCache;
                    $StrCache = "";
                    empty($split);
                }

                $Return = $DateCache . " น.";
                return $Return;
            }
        }
    }
    public function clean($text = '', $replace = ' ', $lenght = 0)
    {
        $text = htmlspecialchars_decode($text);
        $nonechar = array('&#039;', '~', "'", '"', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '=', '-', '{', '}', '[', ']', '<', '>', '?', ',', '/', '|', '\\', ':', ';', '.', ',', ' ');
        $text = str_replace($nonechar, $replace, strip_tags($text));
        if ($lenght > 0) {
            $text = mb_substr($text, 0, $lenght, 'UTF-8');
        }
        $text = preg_replace('/\s+/', $replace, $text);
        return $text;
    }

    public function trim_detail($text, $len = 100)
    {
        return $this->unwraptext($text, $len) . (strlen($text) > $len ? '&hellip;' : '');
    }

    public function unwraptext($text, $len = 0)
    {
        $text = str_replace(array('<br />', '<br>', '&nbsp;'), ' ', $text);
        $text = strip_tags($text);
        $text = preg_replace('/[\t\r\n]+/', ' ', $text);
        $text = preg_replace('/\s\s+/', ' ', $text);
        if ($len > 0) {
            $text = mb_substr($text, 0, $len, 'UTF-8');
        }
        $text = trim($text);

        return $text;
    }

    public function paginator($link, $page, $items, $limit = 10, $length = 3)
    {
        $pages = ceil($items / $limit);
        if ($pages <= 0) {
            $pages = 1;
        }

        $p_link = '<a class="btn btn-default" href="{{LINK}}">{{TEXT}}</span></a>';
        $p_active = '<a class="btn btn-primary" href="{{LINK}}">{{TEXT}}</span></a>';
        $p_disabled = '<button class="btn btn-default" disabled="" type="button">{{TEXT}}</button>';

        $t_prev = '<span class="glyphicon glyphicon-chevron-left"></span> Prev';
        $t_next = 'Next <span class="glyphicon glyphicon-chevron-right"></span>';

        $show = '';
        if ($page <= ($length * 2)) {
            if ($page == 1) {
                $show .= str_replace('{{TEXT}}', $t_prev, $p_disabled);
            } else {
                $link_page = str_replace('{{PAGE}}', $page - 1, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $t_prev), $p_link);
            }
            for ($i = 1; $i < $page; $i++) {
                $link_page = str_replace('{{PAGE}}', $i, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $i), $p_link);
            }
            $link_page = str_replace('{{PAGE}}', $page, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $page), $p_active);

            $start = $page + 1;
            if ($page + $length >= $pages) {
                for ($i = $start; $i <= $pages; $i++) {
                    $link_page = str_replace('{{PAGE}}', $i, $link);
                    $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $i), $p_link);
                }
            } else {
                $end = $page + $length;
                for ($i = $start; $i <= $end; $i++) {
                    $link_page = str_replace('{{PAGE}}', $i, $link);
                    $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $i), $p_link);
                }
                $show .= str_replace('{{TEXT}}', '&hellip;', $p_disabled);
                $link_page = str_replace('{{PAGE}}', $pages, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $pages), $p_link);
            }
            if ($page == $pages) {
                $show .= str_replace('{{TEXT}}', $t_next, $p_disabled);
            } else {
                $link_page = str_replace('{{PAGE}}', $page + 1, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $t_next), $p_link);
            }
        } elseif ($page > $pages - ($length * 2)) {
            $link_page = str_replace('{{PAGE}}', $page - 1, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $t_prev), $p_link);
            $link_page = str_replace('{{PAGE}}', 1, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, 1), $p_link);
            $show .= str_replace('{{TEXT}}', '&hellip;', $p_disabled);
            $start = $page - $length;
            $end = $page - 1;
            for ($i = $start; $i <= $end; $i++) {
                $link_page = str_replace('{{PAGE}}', $i, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $i), $p_link);
            }
            $link_page = str_replace('{{PAGE}}', $page, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $page), $p_active);
            $start = $page + 1;
            for ($i = $start; $i <= $pages; $i++) {
                $link_page = str_replace('{{PAGE}}', $i, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $i), $p_link);
            }
            if ($page == $pages) {
                $show .= str_replace('{{TEXT}}', $t_next, $p_disabled);
            } else {
                $link_page = str_replace('{{PAGE}}', $page + 1, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $t_next), $p_link);
            }
        } else {
            $link_page = str_replace('{{PAGE}}', $page - 1, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $t_prev), $p_link);
            $link_page = str_replace('{{PAGE}}', 1, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, 1), $p_link);
            $show .= str_replace('{{TEXT}}', '&hellip;', $p_disabled);
            $start = $page - $length;
            $end = $page - 1;
            for ($i = $start; $i <= $end; $i++) {
                $link_page = str_replace('{{PAGE}}', $i, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $i), $p_link);
            }
            $link_page = str_replace('{{PAGE}}', $page, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $page), $p_active);
            $start = $page + 1;
            $end = $page + $length;
            for ($i = $start; $i <= $end; $i++) {
                $link_page = str_replace('{{PAGE}}', $i, $link);
                $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $i), $p_link);
            }
            $show .= str_replace('{{TEXT}}', '&hellip;', $p_disabled);
            $link_page = str_replace('{{PAGE}}', $pages, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $pages), $p_link);
            $link_page = str_replace('{{PAGE}}', $page + 1, $link);
            $show .= str_replace(array('{{LINK}}', '{{TEXT}}'), array($link_page, $t_next), $p_link);
        }
        return $show;
    }

    public function curl($url, $post = false, $header = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if ($header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        if ($post) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        }
        $contents = curl_exec($ch);
        curl_close($ch);
        return $contents;
    }

    public function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function base64url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    public function dateth($pattern, $time)
    {
        /*
        Day
        d =>     01 to 31
        j =>     1 to 31
        D =>     Mon through Sun
        l =>     Sunday through Saturday
        w =>     0 (for Sunday) through 6 (for Saturday)

        Month
        m =>     01 through 12
        n =>     1 through 12
        M =>     Jan through Dec
        F =>     January through December

        Time
        G =>  0 through 23
        H =>  00 through 23
        i =>  00 to 59
        s =>  00 through 59
         */
        $return = date($pattern, $time);

        if (strpos($pattern, 'D') !== false) {
            $find = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
            $replace = array('อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.');
            $return = str_replace($find, $replace, $return);
        }

        if (strpos($pattern, 'l') !== false) {
            $find = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            $replace = array('อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์');
            $return = str_replace($find, $replace, $return);
        }

        if (strpos($pattern, 'M') !== false) {
            $find = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
            $replace = array('ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');
            $return = str_replace($find, $replace, $return);
        }

        if (strpos($pattern, 'F') !== false) {
            $find = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            $replace = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
            $return = str_replace($find, $replace, $return);
        }

        if (strpos($pattern, 'Y') !== false) {
            $AD = date('Y', $time);
            $BE = $AD + 543;
            $return = str_replace($AD, $BE, $return);
        }

        return $return;
    }

    public function getQuarter()
    {
        $num = ceil(date("n")  / 3) - 1;
        $quarter = ($num < 3) ?  $num + 2 :  $num - 2;
        $date = $this->getMonths($quarter);
        return array(
            "year" => $date['year'],
            "quarter" => $quarter,
            "month" => $date['month'],
            "dateEnd" => $date['dateEnd']
        );
    }
    public function getMonths($quarter)
    {
        switch ($quarter) {
            case 3:
                return array(
                    "month" => array("JANUARY", "FEBRUARY", "MARCH"),
                    "dateEnd" => 31,
                    "monthPay" => "MAY",
                    "yearPay" => date("Y"),
                    "year" => date("Y")
                );
            case 4:
                return array(
                    "month" => array("APRIL", "MAY", "JUNE"),
                    "dateEnd" => 30,
                    "monthPay" => "AUGUST",
                    "yearPay" => date("Y"),
                    "year" => date("Y")
                );
            case 1:
                return array(
                    "month" => array("JULY", "AUGUST", "SEPTEMBER"),
                    "dateEnd" => 30,
                    "monthPay" => "NOVEMBER",
                    "yearPay" => date("Y"),
                    "year" => date("Y") + 1
                );
            case 2:
                return array(
                    "month" => array("OCTOBER", "NOVEMBER", "DECEMBER"),
                    "dateEnd" => 31,
                    "monthPay" => "FEBRUARY",
                    "yearPay" => date("Y") + 1,
                    "year" => date("Y") + 1
                );
        }
    }
}
