<?php
/*
*******************************************************************

Model Class 1st - 21/6/2015

*******************************************************************

Example 1 : SELECT
// Table = member
// Primary Key = member_id
// $member_id
$member = $this->model->get('member',$member_id);
// Modify Array Return @ protected function get_{$table}($return) 

*******************************************************************

Example 2 : SELECT
// Table = torrent_group
// Primary Key = group_id
// $group_id
$member = $this->model->get('torrent_group,group_id',$group_id);
// Modify Array Return @ protected function get_{$table}($return) 

*******************************************************************

Example 3 : UPDATE
$table = 'member';
$id = '123';
$member = $this->model->get($table,$id);
$uploaded = $member['member_uploaded'] + 999999;
$this->db->update("
	UPDATE
		{{PREFIX}}_member
	SET
		member_uploaded = '{$uploaded}'
	WHERE
		member_id = {$member['member_id']}
");
$member['member_uploaded'] = $uploaded;
$this->model->set($table,$id,$member);
// Use in every UPDATE TABLE

*******************************************************************

Should not use $this->model->del($table,$id);

*******************************************************************
*/
final class Model {
	
	protected $prefix;
	
    public function __construct()
    {
    	$this->prefix = DB_PREFIX;
    }
    
	public function __get($key)
	{
		$this->$key = Registry::get($key);
		return $this->$key;
	}
	
	protected function fetch($select = '*',$from = '', $where = array('None','Null'))
	{
		
		$return = $this->db->fetch($this->db->select("
			SELECT
				{$select}
			FROM
				{$this->prefix}_{$from}
			WHERE
				{$where[0]} = '{$where[1]}'
		"));
		return $return;
	}
	
	public function get($table = FALSE, $id = 0, $get = TRUE)
	{
		$return = FALSE;
		$table = explode(',',$table);
		$table_name = $table[0];
		
		$primary_key = $table_name.'_id';
		if(isset($table[1]))
		{
			$primary_key = $table[1];
		}
		
		if($id>0)
		{
			$cacheKey = $table_name.'-'.$id;
			if($get)
			{
				if(!$return = $this->cache->get($cacheKey))
				{
					if($return = $this->fetch('*',$table_name,array($primary_key,$id)))
					{
						
						$func = 'get_'.$table_name;
						$return = $this->$func($return);
						$return['_cache'] = date('d/m/Y - H:i:s',$_SERVER['REQUEST_TIME']);
						$this->set($table_name,$id,$return);
					}
				}
			}
			else
			{
				$this->cache->delete($cacheKey);
			}
		}
		return $return;
	}
	
	public function set($table = FALSE, $id = 0, $data = array())
	{
		$table = explode(',',$table);
		$table_name = $table[0];
		if($id==0)
		{
			$id = $data['id'];
		}
		$this->cache->set($table_name.'-'.$id,$data);
	}
	
	public function del($table = FALSE, $id = 0)
	{
		$table = explode(',',$table);
		$table_name = $table[0];
		$this->cache->delete($table_name.'-'.$id);
	}
	
	/* ********** Get Pattern ********** */
	
	public function columns($table,$db='database')
	{
		$return = array();
		$columns = $this->db->select("
			SELECT
				COLUMN_NAME
			FROM
				INFORMATION_SCHEMA.COLUMNS
			WHERE
				TABLE_SCHEMA = '{$db}' AND 
				TABLE_NAME = '{$table}'
		");
		while($column = $this->db->fetch($columns))
		{
			$return[] = $column['COLUMN_NAME'];
		}
		return $return;
	}
	
	public function get_model($table,$replace = false)
	{
		$column = $this->columns($table,'dooball');
		if(!$replace){
			$replace = explode('_',$column[0]);
			$replace = $replace[0].'_';
		}
		$return = "";
		foreach($column as $key => $val) {
			$k = str_replace($replace,'',$val);
			$return .= "'{$k}' => \$fetch['{$val}'], <br />";
		}
		return $return;
	}
	
	/* ********** Return Array ********** */
	
	
	public function get_member($fetch)
	{
		return $fetch;
	}
	
	public function get_group($fetch) {
		$n = 'group';
		$return = array();
		foreach($fetch as $key => $val) {
			$return[str_replace($n.'_','',$key)] = $val;
		}
		return $return;
	}
	
	public function get_categories($fetch) {
		$n = 'categories';
		$return = array();
		foreach($fetch as $key => $val) {
			$return[str_replace($n.'_','',$key)] = $val;
		}
		$list = explode(',',$return['list']);
		$return['list'] = array();
		foreach($list as $key => $val) {
			$return['list'][$val] = '1';
		}
		return $return;
	}
	
	public function get_content($fetch) {
		$n = 'content';
		$return = array();
		foreach($fetch as $key => $val) {
			$return[str_replace($n.'_','',$key)] = $val;
		}
		return $return;
	}
	
	public function get_source($fetch) {
		$n = 'source';
		$return = array();
		foreach($fetch as $key => $val) {
			$return[str_replace($n.'_','',$key)] = $val;
		}
		if(!$return['data']) {
			$return['data'] = array();
		} else {
			$return['data'] = json_decode($return['data'],true);
		}
		$return['m3u8'] = "http://{$_SERVER['HTTP_HOST']}/VOD/{$return['id']}/playlist.m3u8";
		return $return;
	}
	public function get_server($fetch) {
		$n = 'server';
		$return = array();
		foreach($fetch as $key => $val) {
			$return[str_replace($n.'_','',$key)] = $val;
		}
		return $return;
	}
	
}

?>
