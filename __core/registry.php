<?php
final class Registry {
	static private $class = array();
	static public $map = array();
	
	static public function get($key)
	{
		if(!isset(self::$class[$key]))
		{
			$path = $key;
			if(isset(self::$map[$key]))
			{
				$path = self::$map[$key];
			}
			require(CORE_PATH .$path.'.php');
			self::$class[$key] = new $key();
		}
		
		return self::$class[$key];
	}
}
?>