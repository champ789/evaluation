<?php
final class Request {
	public $get = array();
	public $post = array();
	
	public function __get($key)
	{
		$this->$key = Registry::get($key);
		return $this->$key;
	}
	
	public function __construct() {
		// $this->db = $db;
		
		$xmlRequest = json_decode(file_get_contents("php://input"),true);
		if($xmlRequest) {
			foreach($xmlRequest as $key => $val) {
				$_POST[$key] = $val;
			}
		}
		$request_get = $this->clean($_GET);
		$request_post = $this->clean($_POST);
		
		$this->get = $request_get;
		$this->post = $request_post;
		// $this->server = $_SERVER;
	}
	
	public function clean($data) {
		if (is_array($data)) {
			foreach ($data as $key => $value) {
			unset($data[$key]);
			
				$data[$this->clean($key)] = $this->clean($value);
			}
		} else {
			$data = trim($data);
			$data = str_replace('\\','',$data);
			$data = htmlentities($data, ENT_QUOTES, 'UTF-8');
			$data = $this->db->escape($data);
		}
		
		return $data;
	}
}
?>