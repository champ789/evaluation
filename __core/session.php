<?php
require_once(dirname(__FILE__).'/Zend/Session.php');
final class Session extends Zend_Session_Namespace {
	
	public function __construct($site='default') {
		parent::__construct(SITE, false);
	}
	
	public function rememberMe($sec)
	{
		Zend_Session::rememberMe($sec);
	}
	
	public function destroy()
	{
		Zend_Session::destroy();
	}
}
?>