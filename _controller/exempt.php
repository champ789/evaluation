<?php

class ControllerExempt extends Controller
{
    public function index()
    {
        if (isset($this->request->get['data'])) {
            $this->getData();
        }
        if (isset($this->request->post['saveScore'])) {
            $this->saveScore();
        }
        if (isset($this->request->post['approve'])) {
            $this->approve();
        }
        if (isset($this->request->post['deptSend'])) {
            $this->deptSend();
        }
        if (isset($this->request->get['exportExcel'])) {
            $this->getEmp();
        }
        $date = $this->functions->getMonths($_SESSION['user']['quarter']);
        $this->data['date'] = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter'],
            "month" => $date['month'],
            "monthPay" => $date['monthPay'],
            "dateEnd" => $date['dateEnd']
        );
        $this->render(true);
    }

    protected function getData()
    {
        $date = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter']
        );
        $return = array("seeOnly" => false);
        $en = 0;
        if (isset($this->request->get['en'])) {
            $en = $this->request->get['en'];
            $return['seeOnly'] = true;
        }
        $manager = $_SESSION['user'];
        $sql = "
			SELECT
                e.*,m.name as manager,d.name as dept,s.*,m.status as mStatus
			FROM
                employee e
            RIGHT JOIN
                manager m ON m.en = e.manager_en AND m.quarter=e.quarter AND m.year = e.year
            RIGHT JOIN
                score s ON s.en = e.en AND s.quarter=e.quarter AND s.year = e.year
            RIGHT JOIN
                dept d   ON d.en = e.dept_en AND d.quarter=e.quarter and d.year = e.year
			WHERE
                e.year = {$date['year']} AND
                e.quarter = {$date['quarter']}
        ";
        if ($en) {
            // $sql .= "
            //     AND e.dept_en = '{$manager['en']}'
            //     AND e.manager_en = '${en}'
            // ";
            $sql .= "
                AND e.manager_en = '${en}'
            ";
        } else {
            $sql .= "
                AND e.manager_en = '{$manager['en']}'
            ";
        }
        $sql .= "
            ORDER BY
                e.level,e.en
        ";
        $query = $this->db->select($sql);
        while ($fetch = $this->db->fetch($query)) {
            $lv = substr($fetch['level'], 0, 2);
            if (in_array($lv, array('B1', 'B2'))) {
                $fetch['lv'] = 'exem';
            } else if (in_array($lv, array('B3', 'B4', 'B5'))) {
                $fetch['lv'] = 'mgr';
            }
            if (in_array(substr($fetch['level'], 0, 2), array('B1', 'B2'))) {
                $fetch['lv'] = 'exem';
            }

            $fetch['chkCost'] = in_array($fetch['cost_center'], array('0618', '0619', '8618'));
            $fetch['other'] = unserialize($fetch['other']);
            // $fetch['mStatus'] = 1;
            $return['mStatus'] = +$fetch['mStatus'];
            $return['data'][] = $fetch;
        }
        echo json_encode($return);
        exit();
    }

    protected function saveScore()
    {
        $data = $this->request->post['data'];
        $sql = "
            UPDATE
                score
            SET
                score1 = '{$data['score1']}',
                score2 = '{$data['score2']}',
                score3 = '{$data['score3']}',
                score4 = '{$data['score4']}',
                score5 = '{$data['score5']}',
                total = '{$data['total']}',
                grade = '{$data['grade']}'
            WHERE
                en = '{$data['en']}' AND
                year = {$data['year']} AND
                quarter = {$data['quarter']}
        ";
        $this->db->update($sql);
        echo json_encode(array("success" => true));
        exit();
    }

    protected function approve()
    {
        $date = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter']
        );
        $manager = $_SESSION['user'];
        $sqlCheckDept = "
            SELECT
                *
            FROM
                manager
            WHERE
                year = {$date['year']} AND
                quarter = {$date['quarter']} AND
                en = '{$manager['en']}'
            LIMIT
                1
        ";
        $query = $this->db->select($sqlCheckDept);
        if (!$this->db->numrow($query)) {
            echo json_encode(array("error" => "Data not found !!"));
            exit();
        }
        $mgr = $this->db->fetch($query);
        $status = 1;
        if ($mgr['en'] == $mgr['dept_en']) { //คนเดียวกัน
            $status = 3;
        }
        $sqlUpdate = "
            UPDATE
                manager
            SET
                status = {$status}
            WHERE
                year = {$date['year']} AND
                quarter = {$date['quarter']} AND
                en = '{$mgr['en']}'
        ";
        $this->db->update($sqlUpdate);
        if ($status == 1) {
            $sqlSelectDept = "
                SELECT
                    *
                FROM
                    dept
                WHERE
                    year = {$date['year']} AND
                    quarter = {$date['quarter']} AND
                    en = '{$mgr['dept_en']}'
                LIMIT
                    1
            ";
            $dept = $this->db->fetch($this->db->select($sqlSelectDept));
            $this->sendMail('dept', $dept['email'], $dept['name'], $mgr['name']);
        }
        echo json_encode(array("success" => true, "status" => $status));
        exit();
    }

    protected function deptSend()
    {
        $date = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter']
        );
        $dept = $_SESSION['user'];
        $mgr = $this->request->get['en'];
        $type = $this->request->post['deptSend'];
        if ($type == "Approve") {
            $status = 3;
        } else if ($type == "Reject") {
            $status = 2;
        } else {
            $status = 1;
        }
        $sqlCheckDept = "
            SELECT
                *
            FROM
                manager
            WHERE
                year = {$date['year']} AND
                quarter = {$date['quarter']} AND
                en = '{$mgr}' AND
                dept_en = '{$dept['en']}'
            LIMIT
                1
        ";
        $query = $this->db->select($sqlCheckDept);
        if (!$this->db->numrow($query)) {
            echo json_encode(array("error" => "Data not found !!"));
            exit();
        }
        $manager = $this->db->fetch($query);
        $sqlUpdate = "
            UPDATE
                manager
            SET
                status = {$status}
            WHERE
                year = {$date['year']} AND
                quarter = {$date['quarter']} AND
                en = '{$mgr}' AND
                dept_en = '{$dept['en']}'
        ";
        $this->db->update($sqlUpdate);
        $this->sendMail($type, $manager['email'], $manager['name']);
        echo json_encode(array("success" => true));
        exit();
    }

    protected function sendMail($type, $email, $name, $mgrName = '')
    {
        $data = array();
        $sql = "
            SELECT
                *
            FROM
                other
        ";
        $select = $this->db->select($sql);
        while ($f = $this->db->fetch($select)) {
            $data[$f['subject']] = $f['value'];
        }
        $subject = $data['subject'];
        $message = "";
        switch ($type) {
            case "dept": //ส่ง approve
                $message = $data['msgToDept'];
                $message = str_replace(array("DEPT_NAME", 'MANAGER_NAME'), array($name, $mgrName), $message);
                break;
            case "Approve": //dept approve
                $message = $data['msgDeptApprove'];
                $message = str_replace("MANAGER_NAME", $name, $message);
                break;
            case "Reject": //dept reject
                $message = $data['msgDeptReject'];
                $message = str_replace("MANAGER_NAME", $name, $message);
                break;
        }
        $name = $data['nameSender'];
        $emailSender = $data['email'];
        $mail = $this->phpmailler->sendMail($subject, $message, $email, $emailSender, $name);
        return $mail;
    }

    protected function getEmp()
    {
        $date = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter']
        );
        $manager = $_SESSION['user'];
        $sql = "
            SELECT
                e.*,m.name as manager,d.name as dept,s.*
            FROM
                employee e
            RIGHT JOIN
                manager m ON m.en = e.manager_en AND m.quarter=e.quarter AND m.year = e.year
            RIGHT JOIN
                score s ON s.en = e.en AND s.quarter=e.quarter AND s.year = e.year
            RIGHT JOIN
                dept d   ON d.en = e.dept_en AND d.quarter=e.quarter and d.year = e.year
            WHERE
                e.year = {$date['year']} AND
                e.quarter = {$date['quarter']} AND
                e.manager_en = '{$manager['en']}'
            ORDER BY
                manager,e.level,e.en
        ";
        $query = $this->db->select($sql);
        while ($fetch = $this->db->fetch($query)) {
            for ($i = 1; $i <= 5; $i++) {
                if (!isset($fetch['score' . $i]) || $fetch['score' . $i] < 0) {
                    $fetch['score' . $i] = 0;
                }
            }
            if (in_array(substr($fetch['level'], 0, 2), array('B1', 'B2'))) {
                $fetch['lv'] = 'exem';
            } else {
                $fetch['lv'] = 'mgr';
            }
            $return['data'][] = $fetch;
        }
        $return['year'] = $date['year'];
        $return['quarter'] = $date['quarter'];
        $_SESSION['eva']['excel'] = $return;
        echo json_encode(array("succes" => $return));
        exit();
    }
}
