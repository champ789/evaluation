<?php
class ControllerHome extends Controller
{
    public function index()
    {

        $this->render(true);
    }

    protected function get_group()
    {
        $return = array();
        $sql = "
			SELECT
				group_id
			FROM
				{{PREFIX}}_group
			WHERE
				group_status = '1'
		";
        $query = $this->db->select($sql);
        while ($fetch = $this->db->fetch($query)) {
            $return[] = $this->model->get('group', $fetch['group_id']);
        }
        // print_r($return);exit()
        return $return;
    }
}
