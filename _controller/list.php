<?php
class ControllerList extends Controller
{
    public function index()
    {
        if (isset($this->request->get['data'])) {
            $this->getData();
        }
        if (isset($this->request->get['exportExcel'])) {
            $this->getEmp();
        }
        $this->render(true);
    }

    protected function getData()
    {
        $date = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter']
        );
        $return = array();
        $sql = "
			SELECT
               *
			FROM
                manager
			WHERE
                year = {$date['year']} AND
                quarter = {$date['quarter']}
            ORDER BY
                name
        ";
        $query = $this->db->select($sql);
        while ($fetch = $this->db->fetch($query)) {
            // $fetch['status'] = rand(0, 3);
            $return['data'][] = $fetch;
        }
        $return['listYear'] = $this->getListData();
        $return['date'] = $date;
        echo json_encode($return);
        exit();
    }

    protected function getEmp()
    {
        $date = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter']
        );
        $year = $this->request->get['year'];
        $quarter = $this->request->get['quarter'];
        $sql = "
            SELECT
                e.*,m.name as manager,d.name as dept,s.*
            FROM
                employee e
            RIGHT JOIN
                manager m ON m.en = e.manager_en AND m.quarter=e.quarter AND m.year = e.year
            RIGHT JOIN
                score s ON s.en = e.en AND s.quarter=e.quarter AND s.year = e.year
            RIGHT JOIN
                dept d   ON d.en = e.dept_en AND d.quarter=e.quarter and d.year = e.year
            WHERE
                e.year = {$year} AND
                e.quarter = {$quarter}
            ORDER BY
                manager,e.level,e.en
        ";
        $query = $this->db->select($sql);
        while ($fetch = $this->db->fetch($query)) {
            for ($i = 1; $i <= 5; $i++) {
                if (!isset($fetch['score' . $i]) || $fetch['score' . $i] < 0) {
                    $fetch['score' . $i] = 0;
                }
            }
            if (in_array(substr($fetch['level'], 0, 2), array('B1', 'B2'))) {
                $fetch['lv'] = 'exem';
            } else {
                $fetch['lv'] = 'mgr';
            }
            $return['data'][] = $fetch;
        }
        $return['year'] = $year;
        $return['quarter'] = $quarter;
        $_SESSION['eva']['excel'] = $return;
        echo json_encode(array("succes" => $return));
        exit();
    }

    protected function getListData()
    {
        $return = array();
        $sql = "
            SELECT
                year,quarter
            FROM
                score
            GROUP BY
                year,quarter
            ORDER BY
                1,2
        ";
        $query = $this->db->select($sql);
        while ($f = $this->db->fetch($query)) {
            $return[$f['year']][] = $f['quarter'];
        }
        return $return;
    }
}
