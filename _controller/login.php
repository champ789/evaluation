<?php
class ControllerLogin extends Controller
{
    public function index()
    {
        if (isset($this->request->get['logout'])) {
            unset($_SESSION['user']);
            header("location: ./CAS/CAS_LOGOUT.php?app=@" . APP_NAME);
            exit();
        }
        CAS_CHECK_SESSION(APP_NAME);
        if ($_SESSION['user_name']) {
            $this->login();
        }
        // else {
        //     header("location: ?action=login");
        //     exit();
        // }

        $this->render(true);
    }

    protected function login()
    {
        $return = array();
        $date = $this->functions->getQuarter();
        $email = CAS_GET_USER_NAME();
        $user = array(
            "en" => false,
            "name" => false,
            "level" => false,
            "header" => "",
            "year" => $date['year'],
            "quarter" => $date['quarter']
        );
        $page = array(
            "exempt" => false,
            "list" => false,
            "manager" => false,
            "setting" => false
        );
        $sql = "
			SELECT
				*
			FROM
                manager
			WHERE
                email = '{$email}' AND
                year = {$date['year']} AND
                quarter = {$date['quarter']} 
			LIMIT	1
        ";
        $check = $this->db->fetch($this->db->select($sql));
        if (isset($check)) { //check เป็น manager ไหม
            $page['exempt'] = true;
            $user['en'] = $check['en'];
            $user['name'] = $check['name'];
            $user['level'] = "manager";
            $user['header'] = "?action=exempt";
        }
        $sql = "
            SELECT
                *
            FROM
                dept
            WHERE
                email = '{$email}' AND
                year = {$date['year']} AND
                quarter = {$date['quarter']} 
            LIMIT	1
        ";

        $check = $this->db->fetch($this->db->select($sql));
        if (isset($check)) { // check dept ไหม
            $checkDeptAndMgr = $this->db->fetch($this->db->select("
                SELECT
                    *
                FROM
                    manager
                WHERE
                    dept_en = '{$check['en']}' AND
                    year = {$date['year']} AND
                    quarter = {$date['quarter']} AND
                    en != dept_en
            "));
            if (isset($checkDeptAndMgr)) { //เป็น dept เเละมี manage และไม่ใช่ตัวเอง
                $page['manager'] = true;
                $page['exempt'] = true;
                $user['en'] = $check['en'];
                $user['name'] = $check['name'];
                $user['level'] = "dept";
                $user['header'] = "?action=manager";
            }
        }

        $sql = "
            SELECT
                *
            FROM
                admin
            WHERE
                email = '{$email}'
            LIMIT 1
        ";
        $check = $this->db->fetch($this->db->select($sql));
        if (isset($check)) {
            $page["exempt"] = true;
            $page["list"] = true;
            $page["setting"] = true;
            $user['en'] = $check['en'];
            $user['name'] = $check['name'];
            $user['level'] = "admin";
            $user['header'] = "?action=setting";
        }
        $user['page'] = $page;
        $_SESSION['user'] = $user;
        if (!$user['en']) {
            CAS_LOGIN(APP_NAME);
        } else {
            header("location: " . $user['header']);
            exit();
        }
    }
}
