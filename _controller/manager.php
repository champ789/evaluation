<?php
class ControllerManager extends Controller
{
    public function index()
    {
        if (isset($this->request->get['data'])) {
            $this->getData();
        }
        // if (isset($this->request->post['saveScore'])) {
        //     $this->saveScore();
        // }
        $this->render(true);
    }

    protected function getData()
    {
        $date = array(
            "year" => $_SESSION['user']['year'],
            "quarter" => $_SESSION['user']['quarter']
        );
        $manager = $_SESSION['user'];

        $return = array();
        $sql = "
			SELECT
               *
			FROM
                manager
			WHERE
                year = {$date['year']} AND
                quarter = {$date['quarter']} AND
                en != dept_en AND
                dept_en = '{$manager['en']}'
            ORDER BY 
                EN
        ";
        $query = $this->db->select($sql);
        while ($fetch = $this->db->fetch($query)) {
            // $fetch['status'] = rand(0, 3);
            $return[] = $fetch;
        }
        echo json_encode($return);
        exit();
    }

    protected function saveScore()
    {
        $data = $this->request->post['data'];
        $sql = "
            UPDATE
                score
            SET
                score1 = '{$data['score1']}',
                score2 = '{$data['score2']}',
                score3 = '{$data['score3']}',
                score4 = '{$data['score4']}',
                score5 = '{$data['score5']}'
            WHERE
                en = '{$data['en']}' AND
                year = {$data['year']} AND
                quarter = {$data['quarter']}
        ";
        $this->db->update($sql);
        echo json_encode(array("success" => true));
        exit();
    }
}
