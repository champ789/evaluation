<?php
class ControllerSetting extends Controller
{
    public function index()
    {
        if (isset($_FILES['files'])) {
            if ($_FILES["files"]["size"] > 0) {

                $this->addData();
            } else {
                echo json_encode(array("error" => true));
                exit();
            }
        }

        if (isset($this->request->get['getData'])) {
            $this->getData();
        }

        if (isset($this->request->post['saveEmail'])) {
            $this->saveEmail();
        }
        if (isset($this->request->post['testEmail'])) {
            $this->sendMail();
        }
        if (isset($this->request->post['deleteYear'])) {
            $this->deleteYear();
        }

        $this->render(true);
    }

    protected function getData()
    {
        $return = array();
        $sql = "
            SELECT
                *
            FROM
                other
        ";
        $select = $this->db->select($sql);
        while ($f = $this->db->fetch($select)) {
            $return[$f['subject']] = $f['value'];
        }

        // $return = array();
        $sql = "
            SELECT
                year,quarter
            FROM
                score
            GROUP BY
                year,quarter
            ORDER BY
                1,2
        ";
        $query = $this->db->select($sql);
        while ($f = $this->db->fetch($query)) {
            $return['year'][$f['year']][] = $f['quarter'];
        }

        echo json_encode($return);
        exit();
    }
    protected function addData()
    {
        $post = $_POST;
        $fileName = $_FILES["files"]["tmp_name"];
        $file = fopen($fileName, 'r');
        $year = $post['year'];
        $quarter = $post['quarter'];
        $sqlCheckData = "
            SELECT
                *
            FROM
                employee
            WHERE
                year = {$year} AND
                quarter = {$quarter}
        ";
        $numrow = $this->db->numrow($this->db->select($sqlCheckData));
        if ($numrow) {
            echo json_encode(array("error" => "มีข้อมูลของปี " . $year . " quarter " . $quarter . " อยู่แล้ว กรุณาลบข้อมูลก่อนทำการเพิ่มใหม่ !!"));
            exit();
        }
        $chk = 0;
        $max = "
            select
                max(id) as max
            from
                employee
        ";
        $manager = array();
        $dept = array();
        $checkFirstRows = true;
        $max = $this->db->fetch($this->db->select($max));
        if (!isset($max['max']) or $max['max'] == 0) {
            $max = 1;
        } else {
            $max = $max['max'] + 1;
        }
        while ($col = fgetcsv($file)) {
            if ($checkFirstRows) {
                $checkFirstRows = false;
                continue;
            }
            $mgr_en = $this->pad($col[10]);
            $dept_en = $this->pad($col[13]);
            $other = serialize(array(
                ["g" => $col['18'], "s" => $col['19']],
                ["g" => $col['20'], "s" => $col['21']],
                ["g" => $col['22'], "s" => $col['23']],
            ));
            $insert = "
                INSERT INTO
                    employee
                SET
                    id = {$max},
                    quarter = {$quarter},
                    year = {$year},
                    en='{$this->pad($col[0])}',
                    name='{$col[2]}',
                    level='{$col[4]}',
                    position='{$col[5]}',
                    cost_center='{$col[6]}',
                    bu='{$col[7]}',
                    manager_en='{$mgr_en}',
                    dept_en='{$dept_en}',
                    atts='{$this->check($col[24])}',
                    qob='{$this->check($col[25])}',
                    other = '{$other}'
            ";

            $this->db->insert($insert);
            $chk++;
            $insert = "
            INSERT INTO
                score
            SET
                year = '{$year}',
                quarter = {$quarter},
                en='{$this->pad($col[0])}'
            ";
            $this->db->insert($insert);
            $max++;
            if (!isset($member[$mgr_en])) {
                $sql = "
                    INSERT INTO
                        manager
                    SET
                        year = {$year},
                        quarter = {$quarter},
                        en='{$mgr_en}',
                        name='{$col[11]}',
                        email='{$col[12]}',
                        dept_en='{$dept_en}'
                ";
                $this->db->insert($sql);
                $manager[$mgr_en] = $mgr_en;
            }
            if (!isset($dept[$dept_en])) {
                $dept[$dept_en] = $dept_en;
                $sql = "
                    INSERT INTO
                        dept
                    SET
                        year = {$year},
                        quarter = {$quarter},
                        en='{$dept_en}',
                        name='{$col[14]}',
                        email='{$col[15]}'
                ";
                $this->db->insert($sql);
            }
        }
        $sqlInset = "
        INSERT INTO `other` (`subject`, `value`) VALUES
            ('email', 'hr@fabrinet.co.th'),
            ('msgDeptApprove', 'Dear. MANAGER_NAME'),
            ('msgDeptReject', 'Dear.MANAGER_NAME'),
            ('msgToDept', 'Dear. DEPT_NAME'),
            ('nameSender', 'FABRINET'),
            ('subject', 'Subject')
        ";
        $this->db->insert($sqlInset);
        echo json_encode(array("success" => true));
        exit();
        // return $this->check($chk, $year);
    }
    protected function pad($str, $len = 6)
    {
        return str_pad($str, $len, "0", STR_PAD_LEFT);
    }

    protected function check($str)
    {
        if (strlen($str) == 0) {
            return "0";
        } else {
            return $str;
        }
    }

    protected function sendMail()
    {
        $return = true;
        $mailTo = $this->request->post['testEmail'];
        $type = $this->request->post['type'];
        $data = array();
        $sql = "
            SELECT
                *
            FROM
                other
        ";
        $select = $this->db->select($sql);
        while ($f = $this->db->fetch($select)) {
            $data[$f['subject']] = $f['value'];
        }
        $subject = $data['subject'];
        $message = "";
        switch ($type) {
            case "dept":
                $message = $data['msgToDept'];
                $message = str_replace("DEPT_NAME", $_SESSION['user']['name'], $message);
                break;
            case "approve":
                $message = $data['msgDeptApprove'];
                $message = str_replace("MANAGER_NAME", $_SESSION['user']['name'], $message);
                break;
            case "reject":
                $message = $data['msgDeptReject'];
                $message = str_replace("MANAGER_NAME", $_SESSION['user']['name'], $message);
                break;
        }
        $name = $data['nameSender'];
        $emailSender = $data['email'];

        $mail = $this->phpmailler->sendMail($subject, $message, $mailTo, $emailSender, $name);
        if ($mail) {
            echo json_encode(array("error" => $mail->ErrorInfo));
            exit();
        } else {
            echo json_encode(array("success" => true));
            exit();
        }
    }

    protected function saveEmail()
    {
        $return = array();
        $data = $this->request->post['data'];
        $msgDeptApprove = $this->request->post['msgDeptApprove'];
        $msgDeptReject = $this->request->post['msgDeptReject'];
        $msgToDept = $this->request->post['msgToDept'];
        foreach (array("email", "subject", "nameSender", "msgDeptApprove", "msgDeptReject", "msgToDept") as $k => $v) {

            switch ($v) {
                case "msgDeptApprove":
                    $value = $msgDeptApprove;
                    break;
                case "msgDeptReject":
                    $value = $msgDeptReject;
                    break;
                case "msgToDept":
                    $value = $msgToDept;
                    break;
                default:
                    $value = $data[$v];
            }

            $sql = "
                UPDATE
                    other
                SET
                    value = '{$value}'
                WHERE
                    subject = '{$v}'
            ";
            $this->db->update($sql);
        }
        echo json_encode(array("success" => true));
        exit();
    }

    protected function deleteYear()
    {
        // $return = array();
        $year = $this->request->post['year'];
        $quarter = $this->request->post['quarter'];
        $list = array("dept", "employee", "manager", "score");
        $count = 0;
        foreach ($list as $k => $v) {
            $sql = "
                delete from
                    {$v}
                where
                    year = {$year} AND
                    quarter = {$quarter}
            ";
            if ($this->db->update($sql)) {
                $count++;
            }
        };
        if ($count == 4) {
            echo json_encode(array("success" => true));
        } else {
            echo json_encode(array("success" => true));
        }
        exit();
    }
    protected function getListData()
    {
        $return = array();
        $sql = "
            SELECT
                year,quarter
            FROM
                score
            GROUP BY
                year,quarter
            ORDER BY
                1,2
        ";
        $query = $this->db->select($sql);
        while ($f = $this->db->fetch($query)) {
            $return[$f['year']][] = $f['quarter'];
        }
        return $return;
    }
}