<!DOCTYPE html>
<html lang="th">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Fabrinet</title>
    <link rel="stylesheet" href="<?php echo STYLE_HOST; ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo STYLE_HOST; ?>css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="<?php echo STYLE_HOST; ?>css/styles.css" />
    <link rel="stylesheet" href="<?php echo STYLE_HOST; ?>css/sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo STYLE_HOST; ?>css/summernote-bs4.min.css">
    <script src="<?php echo STYLE_HOST; ?>js/jquery.min.js"></script>
    <script src="<?php echo STYLE_HOST; ?>js/popper.min.js"></script>
    <script src="<?php echo STYLE_HOST; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo STYLE_HOST; ?>js/vue.min.js"></script>
    <script src="<?php echo STYLE_HOST; ?>js/vuejs-paginate.js"></script>
    <script src="<?php echo STYLE_HOST; ?>js/summernote-bs4.min.js"></script>
    <script src="<?php echo STYLE_HOST; ?>js/sweetalert2.all.min.js"></script>
</head>

<!-- <body>
    <div class="container-fluid" id="app"> -->

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark" style="    z-index: 1039;">
        <a class="navbar-brand" style="color:#FFF">Fabrinet</a>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <span style="color:#FFF;font-weight:800"><?php echo $_SESSION['user']['name']; ?></span>
        </div>
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="#">Activity Log</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="login.html">Logout</a>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav" id="link">
                        <?php $user = $_SESSION['user']; ?>
                        <?php if ($user['level'] == "admin") { ?>
                            <div class="sb-sidenav-menu-heading">Admin</div>
                            <a class="nav-link " href="?action=setting">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Setting
                            </a>
                            <a class="nav-link " href="?action=list">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                All Manager List
                            </a>
                        <?php } ?>
                        <div class="sb-sidenav-menu-heading">Menu</div>
                        <?php if ($user['page']['exempt']) { ?>
                            <a class="nav-link " href="?action=exempt">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Exempt Staff
                            </a>
                        <?php } ?>
                        <?php if ($user['page']['manager']) { ?>
                            <a class="nav-link " href="?action=manager">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Manager List
                            </a>
                        <?php } ?>
                    </div>
                    <div class="sb-sidenav-footer">
                        <!-- <div class="small">Logged in as:</div> -->
                        <a class="nav-link active" href="?action=login&logout">
                            <div class="sb-nav-link-icon" style="color:red"><i class="mdi mdi-logout"></i>
                                Logout</div>
                        </a>
                    </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <?php echo $_wrapper; ?>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">

            </footer>
        </div>
    </div>
</body>
<script>
    $(function() {
        var current = location.search;
        $('#link a').each(function() {
            var $this = $(this);
            if ($this.attr('href').indexOf(current) !== -1) {
                $this.addClass('active');
            }
        })
    })
</script>

</html>