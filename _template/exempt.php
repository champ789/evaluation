<style>
    table#tabled thead th {
        line-height: 22px;
        padding: 1px 5px;
        text-align: center;
        vertical-align: inherit;
    }

    table#tabled thead tr.goal th:nth-child(n+2),
    th.goal {
        background-color: rgb(255 204 204);
    }

    table#tabled thead tr:nth-child(n+3) th {
        background-color: rgb(102 255 255);
        border-left: solid 2px;
        border-right: solid 2px;
        border-bottom: 0px;
    }

    table#tabled thead tr:nth-child(3) th {
        border-top: solid 2px;
        border-bottom: solid 2px;
        line-height: 15px;
    }

    table#tabled thead tr:nth-child(4) th {
        border-top: solid transparent;
        border-bottom: solid 2px;
        line-height: 10px;
    }

    table#tabled thead tr:first-child th:nth-child(n+2) {
        border: solid 2px;
        border-bottom: solid transparent;
    }

    table#tabled thead tr:nth-child(5n+2) th:nth-child(n+1) {
        border: solid 2px;
    }

    table#tabled tbody tr td {
        font-size: 12px;
        line-height: 10px;
        white-space: nowrap;
        font-weight: 600;
        border: solid 1px;
    }

    table#tabled tr td:nth-child(6),
    table#tabled tr td:nth-child(8),
    table#tabled.mgr tr td:nth-child(9),
    table#tabled tr td:nth-child(10) {
        background-color: rgb(255, 255, 90);
    }

    table#tabled tr td:nth-child(7),
    table#tabled tr td:nth-child(9) {
        background-color: rgb(255, 204, 255);
    }

    table#tabled tr td:nth-child(11) {
        background-color: rgb(0, 255, 255);
        font-size: 15px;
        text-align: center;
        font-weight: bold;
    }

    table#tabled tr td:nth-child(12) {
        background-color: rgb(255, 255, 204);
        font-size: 15px;
        text-align: center;
    }

    table#tabled tr td input {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        width: 100%;
        border: 0;
        text-align: center;
        font-weight: 600;
        background: rgba(0, 0, 0, 0.02);
        box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.3);
        color: #000;
        -webkit-transition: all 0.15s ease;
        transition: all 0.15s ease;
        height: 25px;
        margin: -8px 0;
        font-size: 15px;
    }

    table#tabled tr td input:hover {
        background: rgba(0, 0, 0, 0.04);
        box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.5);
    }

    table#tabled tr td input:focus {
        background: rgba(0, 0, 0, 0.05);
        outline: none;
        box-shadow: inset 0 -2px 0 #0077FF;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance: textfield;
    }

    table#tabled tr td input:disabled {
        border-bottom: none;
        box-shadow: 0 0 black;
        background: border-box;
    }

    table#grade {
        font-size: 13px;
        line-height: 25px;
    }

    table#grade thead th {
        background-color: #64acfd;
        font-size: 11px;
        font-weight: 700;
        border: solid 1px;
    }

    table#grade thead th:nth-child(n+5) {
        width: 80px;
    }

    table#grade tbody tr td {
        padding: 0 10px;
        border: solid 1px;
    }

    table#grade tbody tr td:nth-child(3) {
        text-align: left;
    }

    #progressbar {
        padding: 0 20%;
        overflow: hidden;
        color: lightgrey;
    }

    #progressbar .active {
        color: #000
    }

    #progressbar li {
        list-style-type: none;
        font-size: 12px;
        width: 33%;
        float: left;
        position: relative;
        text-align: center;
        z-index: 1
    }


    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 30px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 3px
    }

    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: lightgray;
        position: absolute;
        left: 0;
        top: 25px;
        z-index: -1
    }

    li#start.active:before,
    li#start.active:after {
        background: skyblue
    }

    li#wait.active:before,
    li#wait.active:after {
        background: indigo
    }

    li#edit.active:before,
    li#edit.active:after {
        background: orange
    }

    li#confirm.active:before,
    li#confirm.active:after {
        background: limegreen
    }
</style>
<!-- Array
(
    [year] => 2020
    [quarter] => 3
    [month] => Array
        (
            [0] => Jul
            [1] => Aug
            [2] => Sep
        )

    [dateEnd] => 30
) -->
<style>
    section {
        padding: 20px;
    }

    /* section .section-title {
        text-align: center;
        color: #007b5e;
        margin-bottom: 50px;
        text-transform: uppercase;
    } */
    /*
    #tabs {
        background: #007b5e;
        color: #eee;
    }

    #tabs h6.section-title {
        color: #eee;
    } */

    #tabs .nav-tabs {
        border-bottom: 1px solid #007b5e;
    }

    #tabs .nav-fill .nav-item {
        flex: none;
        width: 200px
    }

    #tabs .nav-tabs .nav-link.active {
        color: #FFF;
        /* border: solid #007b5e 0.5px; */
        background-color: #007b5e;
        border-bottom: 3px #007b5e;
    }

    #tabs .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        color: #f3f3f3;
        background-color: transparent;
        border-color: transparent transparent #f3f3f3;
        font-size: 20px;
        font-weight: bold;
    }

    #tabs .nav-tabs .nav-link {
        border: 1px solid transparent;
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
        color: #000;
        font-weight: 700;
        font-size: 20px;
    }

    #nav-tabContent {
        border: solid 1px #007b5e;
        padding: 20px;
    }

    .dept button {
        width: 100px;
        margin-left: 20px;
        border-radius: 9px 0px;
    }

    .dept button i::before {
        margin-bottom: -4px;
    }
</style>
<div class="pt-4" id="app" v-if="emp">
    <ul id="progressbar">
        <li id="start" v-bind:class="{ active: status >= 0}" class="mdi mdi-lead-pencil"><strong>Evaluating</strong>
        </li>
        <li v-if="status != 2" id="wait" v-bind:class="{ active: status >= 1}" class="mdi mdi-timetable ">
            <strong>Pending approval</strong>
        </li>
        <li v-if="status == 2" id="edit" v-bind:class="{ active: status == 2}" class=" mdi mdi-autorenew"><strong>Return
                to edit</strong></li>
        <li id="confirm" v-bind:class="{ active: status >= 3}" class="mdi mdi-marker-check"><strong>Completed</strong>
        </li>
    </ul> <!-- fieldsets -->
    <!-- Tabs -->
    <section id="tabs">
        <div class="row">
            <div class="col-xs-12 ">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-exem" role="tab" aria-controls="nav-home" aria-selected="true" v-if="emp.filter(i=>i.lv =='exem').length">Exempt Staff</a>
                        <a class="nav-item nav-link " :class="{ active: emp.filter(i=> i.lv=='exem').length === 0 }" id=" nav-profile-tab" data-toggle="tab" href="#nav-mgr" role="tab" aria-controls="nav-profile" aria-selected="false" v-if="emp.filter(i=>i.lv =='mgr').length">Mgr Staff</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-exem" role="tabpanel" aria-labelledby="nav-home-tab" v-if="emp.filter(i=>i.lv =='exem').length">
                        <h6>PERFORMANCE BONUS WORKSHEET FOR
                            Q<?php echo $date['quarter']; ?>'FY<?php echo $date['year']; ?></br />
                            EVALUATION PERIOD :
                            <?php echo $date['month'][0] . ' 1, ' . $date['year'] . ' - ' . $date['month'][2] . ' ' . $date['dateEnd'] . ', ' . $date['year']; ?><br />
                            PAID DATE : <?php echo $date['monthPay']; ?> 20, 2020
                        </h6>
                        <table class="table" id="tabled" style="margin-top:-15px">
                            <thead style="font-size:11px">
                                <tr class="goal">
                                    <th colspan="4" rowspan="2" style="border-bottom: 2px;border-top:0"></th>
                                    <th>ITEM</th>
                                    <th>(1)</th>
                                    <th>(2)</th>
                                    <th>(3)</th>
                                    <th>(4)</th>
                                    <th>(5)</th>
                                    <!-- <th colspan="5"></th> -->
                                </tr>
                                <tr class="goal">
                                    <th class="goal">GOAL / KPI</th>
                                    <th>Organization<br />Objectives</th>
                                    <th>Quality By<br /> Objectives</th>
                                    <th>Continuous<br />Improvement</th>
                                    <th>Attendance<br />Records</th>
                                    <th>Personal<br />Development</th>
                                </tr>
                                <tr>
                                    <th rowspan="2">NO.</th>
                                    <th rowspan="2">EN</th>
                                    <th rowspan="2">NAME</th>
                                    <th rowspan="2">LEVEL</th>
                                    <th rowspan="2">POSITION</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>TOTAL</th>
                                    <th>Performance</th>
                                    <th rowspan="2">BU</th>
                                    <th rowspan="2">MGR.</th>
                                    <th rowspan="2">BU. HEAD</th>
                                </tr>
                                <th>25</th>
                                <th>25</th>
                                <th>20</th>
                                <th>20</th>
                                <th>10</th>
                                <th>SCORE</th>
                                <th>Rating</th>
                            </thead>
                            <tbody class="table-bordered ">
                                <tr v-for="(item,index) in emp.filter(item => item.lv =='exem')">
                                    <td>{{index+1}}</td>
                                    <td>{{item.en}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.level}}</td>
                                    <td style="width:260px">{{item.position}}</td>
                                    <td style="width:100px">
                                        <input type="number" :max="25" @input="validate(item)" :name=`score1` v-model="item['score1']" @change="calScore(item)" :disabled="disableEdit">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="25" @input="validate(item)" :name=`score2` v-model="item['score2']" @change="calScore(item)" :disabled="disableEdit || !item['chkCost']">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="20" @input="validate(item)" :name=`score3` v-model="item['score3']" @change="calScore(item) " :disabled="disableEdit">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="20" @input="validate(item)" :name=`score4` v-model="item['score4']" @change="calScore(item)" disabled="true">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="10" @input="validate(item)" :name=`score5` v-model="item['score5']" @change="calScore(item)" :disabled="disableEdit">
                                    </td>
                                    <td>{{item.total}}</td>
                                    <td>{{item.grade}}</td>
                                    <td style="width:160px">{{item.bu}}</td>
                                    <td style="width:180px">{{item.manager}}</td>
                                    <td style="width:180px">{{item.dept}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <span style="color:blue" class="font-weight-bold ">Attendance Records for
                            Q<?php echo $date['quarter']; ?>'FY<?php echo $date['year']; ?>
                            (<?php echo $date['month'][0] . '-' . $date['month'][2] . ', ' . $date['year']; ?>)</span>
                        <table class="table-bordered text-center tabled " id="grade">
                            <thead>
                                <th>No.</th>
                                <th>EN</th>
                                <th>NAME</th>
                                <th>Level</th>
                                <?php foreach ($date['month'] as $k => $v) { ?>
                                    <th>Grade of <?php echo $v; ?></th>
                                <?php } ?>
                                <?php foreach ($date['month'] as $k => $v) { ?>
                                    <th>Score of <?php echo $v; ?></th>
                                <?php } ?>
                                <th>Score Average</th>
                            </thead>
                            <tbody>
                                <tr v-for="(item,index) in emp.filter(item => item.lv =='exem')">
                                    <td>{{index+1}}</td>
                                    <td>{{item.en}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.level}}</td>
                                    <td v-for="(v,k) in item.other">{{v.g}}</td>
                                    <td v-for="(v,k) in item.other">{{v.s}}</td>
                                    <td>{{item.aver}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row mt-3">
                            <div class="col-8" style="color:blue">
                                <p class="font-weight-bold">Attendance Score Criteria</p>
                                <p style="margin-top:-5px">Grade A (20 score) = Perfect Attendance => No Absent, No
                                    Leave, No Late in each month.<br />
                                    Grade B (10 Score ) = Late not more than 2 times and less than 20 minutes, take any
                                    leaves* not more than 1 days per month.<br />
                                    Grade C (0 Score) = Missed the above condition <br />
                                    *Exclude Annual Leave (AL), Shut down Leave <br /></p>

                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade show " :class="{ active: emp.filter(i=> i.lv=='exem').length === 0 }" id="nav-mgr" role="tabpanel" aria-labelledby="nav-profile-tab" v-if="emp.filter(i=>i.lv =='mgr').length">
                        <h6>PERFORMANCE BONUS WORKSHEET FOR
                            Q<?php echo $date['quarter']; ?>'FY<?php echo $date['year']; ?></br />
                            EVALUATION PERIOD :
                            <?php echo $date['month'][0] . ' 1, ' . $date['year'] . ' - ' . $date['month'][2] . ' ' . $date['dateEnd'] . ', ' . $date['year']; ?><br />
                            PAID DATE : <?php echo $date['monthPay']; ?> 20, 2020
                        </h6>
                        <table class="table mgr" id="tabled" style="margin-top:-15px">
                            <thead style="font-size:11px">
                                <tr class="goal">
                                    <th colspan="4" rowspan="2" style="border-bottom: 2px;border-top:0"></th>
                                    <th>ITEM</th>
                                    <th>(1)</th>
                                    <th>(2)</th>
                                    <th>(3)</th>
                                    <th>(4)</th>
                                    <th>(5)</th>
                                    <!-- <th colspan="5"></th> -->
                                </tr>
                                <tr class="goal">
                                    <th class="goal">GOAL / KPI</th>
                                    <th>Organization<br />Objectives</th>
                                    <th>Quality By<br /> Objectives</th>
                                    <th>Continuous<br />Improvement</th>
                                    <th>Special<br />Assignment</th>
                                    <th>Personal<br />Development</th>
                                </tr>
                                <tr>
                                    <th rowspan="2">NO.</th>
                                    <th rowspan="2">EN</th>
                                    <th rowspan="2">NAME</th>
                                    <th rowspan="2">LEVEL</th>
                                    <th rowspan="2">POSITION</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>SCORE</th>
                                    <th>TOTAL</th>
                                    <th>Performance</th>
                                    <th rowspan="2">BU</th>
                                    <th rowspan="2">MGR.</th>
                                    <th rowspan="2">BU. HEAD</th>
                                </tr>
                                <th>40</th>
                                <th>20</th>
                                <th>20</th>
                                <th>10</th>
                                <th>10</th>
                                <th>SCORE</th>
                                <th>Rating</th>
                            </thead>
                            <tbody class="table-bordered ">
                                <tr v-for="(item,index) in emp.filter(item=>item.lv=='mgr')">
                                    <td>{{index+1}}</td>
                                    <td>{{item.en}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.level}}</td>
                                    <td style="width:260px">{{item.position}}</td>
                                    <td style="width:100px">
                                        <input type="number" :max="40" @input="validate(item)" :name=`score1` v-model="item['score1']" @change="calScore(item)" :disabled="disableEdit">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="20" @input="validate(item)" :name=`score2` v-model="item['score2']" @change="calScore(item)" disabled="true">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="20" @input="validate(item)" :name=`score3` v-model="item['score3']" @change="calScore(item)" :disabled="disableEdit">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="10" @input="validate(item)" :name=`score4` v-model="item['score4']" @change="calScore(item)" :disabled="disableEdit">
                                    </td>
                                    <td style="width:100px">
                                        <input type="number" :max="10" @input="validate(item)" :name=`score5` v-model="item['score5']" @change="calScore(item)" :disabled="disableEdit">
                                    </td>
                                    <td>{{item.total}}</td>
                                    <td>{{item.grade}}</td>
                                    <td style="width:160px">{{item.bu}}</td>
                                    <td style="width:180px">{{item.manager}}</td>
                                    <td style="width:180px">{{item.dept}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-8 text-right p-4" v-show="!seeOnly">
                    <button type="button" class="btn btn-info p-2 font-weight-bold" @click="exprotExcel" v-show="status != 0"><i class=" mdi mdi-file-excel"></i>
                        Export</button>
                    <button class="btn btn-success p-2 font-weight-bold" :disabled="!sendApprove" @click="sendDept">{{nameButton}}</button>
                </div>
                <div class="col-6 text-right p-4 dept" v-show="seeOnly && status==1">
                    <button class="btn btn-success p-2 font-weight-bold" @click="sendAppOrRej('Approve')"><i class="mdi mdi-checkbox-marked-circle mdi-24px"></i><br />Approve</button>
                    <button class="btn btn-danger p-2 font-weight-bold" @click="sendAppOrRej('Reject')"><i class="mdi mdi-close-circle mdi-24px"></i><br />Reject</button>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </section>
    <!-- ./Tabs -->


</div>
<script>
    $(document).ready(function() {
        let vue = new Vue({
            el: '#app',
            data: {
                emp: null,
                fullScore: ['', 25, 25, 20, 20, 10],
                status: null,
                seeOnly: true,
                disableEdit: false,
                nameButton: " "
            },
            created() {
                $.ajax({
                        method: "GET",
                        url: "",
                        data: {
                            data: 1
                        },
                        dataType: 'json'
                    })
                    .then(response => {
                        this.nameButton = (response.data[0].dept_en == response.data[0].manager_en) ? "Submit to HR" : "Send to Dept.Head"
                        this.status = response['mStatus']
                        this.seeOnly = response['seeOnly']
                        this.emp = response.data.map(item => {
                            item.score2 = item.score2 || item.qob
                            // item.score4 = item.score4 || item.atts
                            item.score4 = item.lv === 'exem' ? item.score4 || item.atts : item
                                .score4
                            item.total = this.sum(item)
                            item.grade = this.calGrade(item.total)
                            item.aver = ([...item.other].reduce((pre, cur) => pre + parseFloat(
                                cur['s']) || 0, 0) / 3).toFixed(1)
                            return item
                        })
                    })
                    .catch(error => {
                        console.log(error);
                    });
            },
            methods: {
                validate(item) {
                    let newValue = parseFloat(event.target.value)
                    if (Number.isNaN(newValue)) newValue = 0
                    const clampedValue = Math.min(newValue, event.target.max)
                    item[event.target.name] = clampedValue
                },
                sum(item) {
                    let total = 0
                    for (i = 1; i <= 5; i++) {
                        let score = parseFloat(item[`score${i}`])
                        if (Number.isNaN(score)) score = 0
                        total += score
                    }
                    return total % 1 === 0 ? total : total.toFixed(1)
                },
                calScore(item) {
                    item.total = this.sum(item)
                    item.grade = this.calGrade(item.total)
                    this.autoSaveScore(item)
                },
                calGrade(intTotal) { //คำนวณเกรด
                    let grade = 0
                    switch (true) {
                        case intTotal >= 91:
                            grade = 5;
                            break;
                        case intTotal >= 81:
                            grade = 4;
                            break;
                        case intTotal >= 61:
                            grade = 3;
                            break;
                        case intTotal >= 40:
                            grade = 2;
                            break;
                        case intTotal >= 20:
                            grade = 1;
                            break;
                        default:
                            grade = 0
                    }
                    return grade
                },
                autoSaveScore(item) {
                    for (i = 1; i <= 5; i++) {
                        let score = parseFloat(item[`score${i}`])
                        if (Number.isNaN(score) || score < 0) return;
                    }
                    $.ajax({
                        type: 'POST',
                        url: '',
                        data: {
                            saveScore: 1,
                            data: item
                        },
                        dataType: 'json',
                    }).then(response => {}).catch(error => {})
                },
                sendDept() {
                    for (item of this.emp) {
                        for (i = 1; i <= 5; i++) {
                            let score = parseFloat(item[`score${i}`])
                            if (Number.isNaN(score) || score < 0) {
                                Swal.fire({
                                    type: 'error',
                                    title: 'plese check the score '
                                })
                                return false
                            }
                        }
                    }
                    Swal.queue([{
                        title: 'Are you sure?',
                        text: "Send to Dept,Head",
                        type: 'warning',
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No!',
                        preConfirm: () => {
                            return fetch('', {
                                    method: 'POST', // or 'PUT'
                                    dataType: 'json',
                                    body: JSON.stringify({
                                        approve: 1
                                    }),
                                })
                                .then(response => response.json())
                                .then(data => {
                                    if (data.success === true) {
                                        Swal.insertQueueStep({
                                            title: 'Complete!',
                                            text: 'Send Approve is success',
                                            type: 'success'
                                        })
                                        this.status = data.status
                                    } else {
                                        Swal.insertQueueStep({
                                            position: 'top-center',
                                            type: 'error',
                                            title: `Error ${data.error}`,
                                            showConfirmButton: true,
                                            timer: 2000
                                        })
                                    }
                                })
                                .catch(() => {
                                    Swal.insertQueueStep({
                                        type: 'error',
                                        title: 'Error to Save '
                                    })
                                })
                        }
                    }])
                },
                sendAppOrRej(type) {
                    this.status = type == "Approve" ? 3 : 2
                    Swal.queue([{
                        title: `Are you sure ${type}?`,
                        type: 'warning',
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        confirmButtonColor: 'green',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No!',
                        preConfirm: () => {
                            return fetch('', {
                                    method: 'POST',
                                    dataType: 'json',
                                    body: JSON.stringify({
                                        deptSend: type
                                    }),
                                })
                                .then(response => response.json())
                                .then(data => {
                                    if (data.success === true) {
                                        Swal.insertQueueStep({
                                            title: 'Complete!',
                                            text: `${type} is success`,
                                            type: 'success'
                                        })
                                        this.status = type == "Approve" ? 3 : 2
                                    } else {
                                        Swal.insertQueueStep({
                                            position: 'top-center',
                                            type: 'error',
                                            title: `Error ${data.error}`,
                                            showConfirmButton: true,
                                            timer: 2000
                                        })
                                    }
                                })
                                .catch(() => {
                                    Swal.insertQueueStep({
                                        type: 'error',
                                        title: 'Error to Save '
                                    })
                                })
                        }
                    }])
                },
                exprotExcel() {
                    $.ajax({
                            method: "GET",
                            url: "",
                            data: {
                                exportExcel: 1,
                                year: this.selectYear,
                                quarter: this.selectQuarter
                            },
                            dataType: 'json'
                        })
                        .then(response => {
                            $('#modalExcel').modal('toggle');
                            window.open(`excel`, '_blank')
                        })
                        .catch(error => {
                            console.log(error);
                        });
                }
            },
            computed: {
                sendApprove() {
                    if (![0, 2].includes(this.status)) return false
                    for (item of this.emp) {
                        for (i = 1; i <= 5; i++) {
                            let score = parseFloat(item[`score${i}`])
                            if (Number.isNaN(score) || score < 0) return false;
                        }
                    }
                    return true
                }
            },
            watch: {
                status() {
                    this.disableEdit = ![0, 2].includes(+this.status) || this.seeOnly
                }
            }
        })
    })
</script>