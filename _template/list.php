<style>
table.table tr th,
table.table tr td {
    border-color: #e9e9e9;
    padding: 5px 15px;
    vertical-align: middle;
}

table.table tr th:first-child {
    width: 60px;
}

table.table-striped tbody tr:nth-of-type(odd) {
    background-color: #fcfcfc;
}

table.table-striped.table-hover tbody tr:hover {
    background: #f5f5f5;
}

.table-title {
    color: #fff;
    background: #4b5366;
    padding: 3px 20px;
    margin: 20px -25px 0px;
    border-radius: 3px 3px 0 0;
}

.table-title h2 {
    margin: 5px 0 0;
    font-size: 24px;
}

.pagination {
    float: right;
    margin: 0 0 5px;
}

.pagination li a {
    border: none;
    font-size: 13px;
    min-width: 30px;
    min-height: 30px;
    color: #999;
    margin: 0 2px;
    line-height: 30px;
    border-radius: 2px !important;
    text-align: center;
    padding: 0 6px;
}

.pagination li a:hover {
    color: #666;
}

.pagination li.active a {
    background: #03A9F4;
}

.page-item.active a {
    z-index: 3;
    color: #fff;
    background-color: #2962FF;
    border-color: #2962FF;
}

.pagination li a {
    position: relative;
    display: block;
    padding: 0.5rem 0.75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #7460ee;
    background-color: #fff;
}

.pagination li.active a:hover {
    background: #0397d6;
}

.pagination li.disabled i {
    color: #ccc;
}

.pagination li i {
    font-size: 16px;
    padding-top: 6px
}

.hint-text {
    float: left;
    margin-top: 10px;
    font-size: 13px;
}

.progressScore .d-flex {
    cursor: help;
}

.show-entries select.form-control {
    width: 80px;
    margin: 0 5px;
}

table tbody tr td span.badge {
    font-size: 12px;
}
</style>
<div id="app" class="px-2 mx-5">
    <div class="table-title">
        <div class="row">
            <div class="col-sm-4">
                <h2><b>MANAGER</b> LIST</h2>
            </div>
            <div class="col-sm-6" style="text-align: right;">
                <div class="btn-group btn-group-toggle btn-group-sm align-right" data-toggle="buttons">
                    <label class="btn btn-secondary active badge badge-primary " @click="status =99">
                        <input type="radio" name="options" id="option1" autocomplete="off" checked> All
                        <span>({{mgr.length}})</span>
                    </label>
                    <label class="btn  btn-secondary" :class="classStatus[index]" v-for="(item,index) in classStatus"
                        @click="status =index">
                        <input type="radio" name="options" autocomplete="off"> {{statusDesc[index]}}
                        <span>({{mgr.filter(it=>it.status == index).length}})</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="show-entries">
                    <span>Show</span>
                    <select class="form-control" v-model="page.item" @change="pagination(1)" style="display:inline">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="500">All</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped table-hover">
        <thead class="text-center">
            <tr>
                <th>#</th>
                <th>EN</th>
                <th class="text-left">Name</th>
                <th class="text-left">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(item, index) in resultQuery.slice(page.start,page.end)">
                <td>{{page.start + index+1}}</td>
                <td class="text-center">{{item.en}}</td>
                <td>{{item.name}}</td>
                <td><span :class="classStatus[item.status]">{{statusDesc[item.status]}}</span></td>
                <td class="text-center"> <button class="btn btn-xs btn-primary" @click="view(item.en)"><i
                            class="mdi mdi-book-search"></i>
                        View</button>
                </td>
            </tr>
        </tbody>
    </table>
    <button class="btn btn-success btn-lg" @click="showModal"><i class="mdi mdi-file-excel"></i> Export
        Excel</button>
    <template v-if="page.count > 1">
        <paginate style="float: right; " :force-page="page.page" :page-count="page.count" :page-range="3"
            :margin-pages="2" :click-handler="pagination" :prev-text="'Prev'" :next-text="'Next'"
            :container-class="'pagination'" :page-class="'page-item'">
        </paginate>
    </template>

    <div class="modal fade bd-example-modal-sm" id="modalExcel" tabindex="-1" role="dialog"
        aria-labelledby="modalExcelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 class="modal-title" id="modalExcelLabel">Export Excel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-12 mb-3" style="text-align:center">
                        <img src="<?php echo STYLE_HOST; ?>/img/excel.png" style=" width: 30%;">
                    </div>
                    <div class="input-group mb-3 col-6">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputYear">Year</label>
                        </div>
                        <select class="custom-select" id="inputYear" v-model="selectYear">
                            <option :value="index" v-for="(item,index) in listYear">{{index}}</option>
                        </select>
                    </div>
                    <div class="input-group mb-3 col-6">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputQuarter">Quarter</label>
                        </div>
                        <select class="custom-select" id="inputQuarter" v-model="selectQuarter">
                            <option value="0">--select--</option>
                            <option :value="item" v-for="(item,index) in listQuarter">{{item}}</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="mdi mdi-close-box"></i>Close</button>
                    <button type="button" class="btn btn-success" @click="exportExcel" :disabled="!selectQuarter"><i
                            class="mdi mdi-file-excel"></i>
                        Export</button>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
$(document).ready(function() {
    Vue.component('paginate', VuejsPaginate)
    let vue = new Vue({
        el: '#app',
        data: {
            mgr: [],
            resultQuery: [],
            status: 99,
            classStatus: ['badge badge-warning', 'badge badge-info', 'badge badge-danger',
                'badge badge-success'
            ],
            statusDesc: ['waiting', 'padding Approve', 'reject', 'approved'],
            total: 50,
            listYear: [],
            listQuarter: [],
            selectYear: null,
            selectQuarter: null,
            page: {
                item: 20,
                start: 0,
                end: 20,
                count: 10,
                page: 1
            },
        },
        created() {
            $.ajax({
                    method: "GET",
                    url: "",
                    data: {
                        data: 1
                    },
                    dataType: 'json'
                })
                .then(response => {
                    this.listYear = response.listYear
                    this.mgr = response.data
                    this.resultQuery = this.mgr
                    this.page.count = Math.ceil(response.data.length / this.page.item)
                    this.selectYear = response.date.year
                    this.selectQuarter = response.date.quarter
                })
                .catch(error => {
                    console.log(error);
                });

        },
        methods: {
            view(en) {
                window.open(`?action=exempt&en=${en}`, '_blank')
            },
            pagination: function(pageNum) {
                this.page.start = (pageNum - 1) * this.page.item
                this.page.end = Number(this.page.start) + Number(this.page.item)
                this.page.page = pageNum
                this.total = this.resultQuery.length
                this.page.count = Math.ceil(this.total / this.page.item)
            },
            searchQuery: function() {
                this.resultQuery = this.mgr.filter((item) => {
                    if (this.status != 99) {
                        return item.status == this.status
                    } else {
                        return true
                    }
                })
                this.pagination(1)
            },
            exportExcel: function() {
                $.ajax({
                        method: "GET",
                        url: "",
                        data: {
                            exportExcel: 1,
                            year: this.selectYear,
                            quarter: this.selectQuarter
                        },
                        dataType: 'json'
                    })
                    .then(response => {
                        $('#modalExcel').modal('toggle');
                        window.open(`excel`, '_blank')
                    })
                    .catch(error => {
                        console.log(error);
                    });

            },
            showModal: function() {
                $('#modalExcel').modal('show')
                this.selectQuart = this.selectQuarter
            }
        },
        computed: {},
        watch: {
            status: function() {
                this.total = this.resultQuery.length
                this.page.start = 0
                this.page.end = this.page.item
                this.page.page = 1
                this.page.count = Math.ceil(this.total / this.page.item)
                this.searchQuery()
            },
            selectYear(cur, old) {
                this.listQuarter = this.listYear[cur]
                if (old) {
                    this.selectQuarter = 0
                }
            }
        }
    })
})
</script>