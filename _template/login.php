<style>
    .modal-dialog.modal-notify.modal-info .modal-header {
        background-color: #f9a845;
    }

    .modal-dialog.modal-notify .modal-header {
        border: 0;
        -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    }

    .modal-dialog .modal-content .modal-header {
        border-top-left-radius: .125rem;
        border-top-right-radius: .125rem;
    }

    .modal-dialog.modal-notify .heading {
        padding: .3rem;
        margin: 0;
        font-size: 1.15rem;
        color: #fff;
    }

    .modal-dialog.modal-notify .modal-body {
        padding: 1.5rem;
        color: #616161;
    }

    .flex-center {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        height: 100%;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAlert">Launch modal</button>
<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info" role="document">
        <div class="modal-content text-center">
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">401, UNAUTHORIZED </p>
            </div>
            <div class="modal-body">

                <i class="fas fa-bell fa-4x animated rotateIn mb-4"></i>

                <p>ไม่พบข้อมูลในระบบ หรือหมดเวลาสำหรับการประเมิณเเล้ว กรุณาติดต่อ HR</p>

            </div>
            <div class="modal-footer flex-center">
                <a href="?action=login&logout" class="btn btn-info">LogIn Agian</a>
                <a href="?action=login&logout" type="button" class="btn btn-danger waves-effect">Logout</a>
            </div>
        </div>
    </div>
</div>