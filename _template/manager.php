<style>
    table.table tr th,
    table.table tr td {
        border-color: #e9e9e9;
        padding: 9px 15px;
        vertical-align: middle;
    }

    table.table tr th:first-child {
        width: 60px;
    }

    table.table-striped tbody tr:nth-of-type(odd) {
        background-color: #fcfcfc;
    }

    table.table-striped.table-hover tbody tr:hover {
        background: #f5f5f5;
    }

    .table-title {
        color: #fff;
        background: #4b5366;
        padding: 3px 20px;
        margin: 20px -25px 0px;
        border-radius: 3px 3px 0 0;
    }

    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
    }
</style>
<div id="app" class="px-2 mx-5">
    <div class="table-title">
        <div class="row">
            <div class="col-sm-4">
                <h2><b>MANAGER</b> LIST</h2>
            </div>
        </div>
    </div>
    <table class="table table-striped table-hover">
        <thead class="text-center">
            <tr>
                <th>#</th>
                <th>EN</th>
                <th class="text-left">Name</th>
                <th class="text-left">Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(item, index) in mgr">
                <td>{{index+1}}</td>
                <td class="text-center">{{item.en}}</td>
                <td><b>{{item.name}}<b></td>
                <td><span :class="classStaus[item.status]">{{statusDesc[item.status]}}</span></td>
                <td class="text-center"> <button class="btn btn-sm btn-primary" @click="view(item.en)"><i class="mdi mdi-book-search"></i>
                        View</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {
        let vue = new Vue({
            el: '#app',
            data: {
                mgr: [],
                classStaus: ['badge badge-warning', 'badge badge-info', 'badge badge-danger', 'badge badge-success'],
                statusDesc: ['waiting', 'padding Approve', 'reject', 'approved'],
            },
            created() {
                $.ajax({
                        method: "GET",
                        url: "",
                        data: {
                            data: 1
                        },
                        dataType: 'json'
                    })
                    .then(response => {
                        this.mgr = response
                    })
                    .catch(error => {
                        console.log(error);
                    });
            },
            methods: {
                view(en) {
                    window.open(`?action=exempt&en=${en}`, '_blank')
                }
            },
            computed: {},
            watch: {}
        })
    })
</script>