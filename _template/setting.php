<style>
.box {
    font-family: Roboto, sans-serif;
    color: #0f3c4b;
    background-color: #e5edf1;
    padding: 5rem 1.25rem;
    margin-top: 30px
}

.contain {
    width: 100%;
    max-width: 680px;
    /* 800 */
    text-align: center;
    margin: 0 auto;
}

.contain h1 {
    font-size: 42px;
    font-weight: 300;
    color: #0f3c4b;
    margin-bottom: 40px;
}

.contain h1 a:hover,
.contain h1 a:focus {
    color: #39bfd3;
}

.contain nav {
    margin-bottom: 40px;
}

.contain nav a {
    border-bottom: 2px solid #c8dadf;
    display: inline-block;
    padding: 4px 8px;
    margin: 0 5px;
}

.contain nav a.is-selected {
    font-weight: 700;
    color: #39bfd3;
    border-bottom-color: currentColor;
}

.contain nav a:not(.is-selected):hover,
.contain nav a:not(.is-selected):focus {
    border-bottom-color: #0f3c4b;
}

.contain footer {
    color: #92b0b3;
    margin-top: 40px;
}

.contain footer p+p {
    margin-top: 1em;
}

.contain footer a:hover,
.contain footer a:focus {
    color: #39bfd3;
}

.box {
    font-size: 1.25rem;
    /* 20 */
    background-color: #c8dadf;
    position: relative;
    padding: 100px 20px;
}

.box.has-advanced-upload {
    outline: 2px dashed #92b0b3;
    outline-offset: -10px;

    -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
    transition: outline-offset .15s ease-in-out, background-color .15s linear;
}

.box.is-dragover {
    outline-offset: -20px;
    outline-color: #c8dadf;
    background-color: #fff;
}

.box__dragndrop,
.box__icon {
    display: none;
}

.box.has-advanced-upload .box__dragndrop {
    display: inline;
}

.box.has-advanced-upload .box__icon {
    width: 100%;
    height: 80px;
    fill: #92b0b3;
    display: block;
    margin-bottom: 40px;
}

.box.is-uploading .box__input,
.box.is-success .box__input,
.box.is-error .box__input {
    visibility: hidden;
}

.box__uploading,
.box__success,
.box__error {
    display: none;
}

.box.is-uploading .box__uploading,
.box.is-success .box__success,
.box.is-error .box__error {
    display: block;
    position: absolute;
    top: 50%;
    right: 0;
    left: 0;

    -webkit-transform: translateY(-50%);
    transform: translateY(-50%);
}

.box__uploading {
    font-style: italic;
}

.box__success {
    -webkit-animation: appear-from-inside .25s ease-in-out;
    animation: appear-from-inside .25s ease-in-out;
}

@-webkit-keyframes appear-from-inside {
    from {
        -webkit-transform: translateY(-50%) scale(0);
    }

    75% {
        -webkit-transform: translateY(-50%) scale(1.1);
    }

    to {
        -webkit-transform: translateY(-50%) scale(1);
    }
}

@keyframes appear-from-inside {
    from {
        transform: translateY(-50%) scale(0);
    }

    75% {
        transform: translateY(-50%) scale(1.1);
    }

    to {
        transform: translateY(-50%) scale(1);
    }
}

.box__restart {
    font-weight: 700;
}

.box__restart:focus,
.box__restart:hover {
    color: #39bfd3;
}

.js .box__file {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}

.js .box__file+label {
    max-width: 80%;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
}

.js .box__file+label:hover strong,
.box__file:focus+label strong,
.box__file.has-focus+label strong {
    color: #39bfd3;
}

.js .box__file:focus+label,
.js .box__file.has-focus+label {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px;
}


.no-js .box__file+label {
    display: none;
}

.no-js .box__button {
    display: block;
}

.box__button {
    font-weight: 700;
    color: #e5edf1;
    background-color: #39bfd3;
    display: block;
    padding: 5px 10px;
    margin: 10px auto;
}

.box__button:hover,
.box__button:focus {
    background-color: #0f3c4b;
}

#tabs .nav-tabs {
    border-bottom: 1px solid #007b5e;
}

#tabs .nav-fill .nav-item {
    flex: none;
    width: 200px
}

#tabs .nav-tabs .nav-link.active {
    color: #FFF;
    /* border: solid #007b5e 0.5px; */
    background-color: #31ae2f;
    border-bottom: 4px #31ae2f;
}

#tabs .nav-tabs .nav-item.show .nav-link,
.nav-tabs .nav-link.active {
    color: #f3f3f3;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    font-size: 20px;
    font-weight: bold;
}

#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #000;
    font-weight: 700;
    font-size: 20px;
}

#nav-tabContent {
    /* border: solid 1px #007b5e; */
    padding: 20px;
    width: 100%
}
</style>

<div class="row" id="app">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">ตั้งค่าการใช้งาน</h4>
                <section id="tabs">
                    <div class="row">
                        <div class="col-md-12">
                            <nav>
                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                        href="#add-data" role="tab" aria-controls="nav-home"
                                        aria-selected="true">Add-Data</a>
                                    <a class="nav-item nav-link " id="nav-home-tab" data-toggle="tab" href="#email"
                                        role="tab" aria-controls="nav-home" aria-selected="true">Email</a>
                                    <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#delete"
                                        role="tab" aria-controls="nav-home" aria-selected="true">Delete Data</a>
                                </div>
                            </nav>
                        </div>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="add-data" role="tabpanel"
                                aria-labelledby="add-data-tab">
                                <div class="contain js" role="main">
                                    <form method="post" action="" enctype="multipart/form-data" novalidate class="box">
                                        <div class="box__input">
                                            <svg class="box__icon" xmlns="http://www.w3.org/2000/svg" width="50"
                                                height="43" viewBox="0 0 50 43">
                                                <path
                                                    d="M48.4 26.5c-.9 0-1.7.7-1.7 1.7v11.6h-43.3v-11.6c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v13.2c0 .9.7 1.7 1.7 1.7h46.7c.9 0 1.7-.7 1.7-1.7v-13.2c0-1-.7-1.7-1.7-1.7zm-24.5 6.1c.3.3.8.5 1.2.5.4 0 .9-.2 1.2-.5l10-11.6c.7-.7.7-1.7 0-2.4s-1.7-.7-2.4 0l-7.1 8.3v-25.3c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v25.3l-7.1-8.3c-.7-.7-1.7-.7-2.4 0s-.7 1.7 0 2.4l10 11.6z" />
                                            </svg>
                                            <input type="file" name="files" id="file" class="box__file"
                                                data-multiple-caption="{count} files selected" multiple />
                                            <label for="file"><strong>Choose a file</strong><span
                                                    class="box__dragndrop"> or
                                                    drag it
                                                    here</span>.</label><br />
                                            <div id="upLoad" style="display:none;">
                                                <div class="row justify-content-md-center">
                                                    <div class="input-group col-4">
                                                        <div class="input-group-prepend">
                                                            <label class="input-group-text">Year</label>
                                                        </div>
                                                        <select class="custom-select" v-model="yearSelect" name="year">
                                                            <option v-for="i in optionYear">{{i}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group col-4" style="margin-left: -25px;">
                                                        <div class="input-group-prepend">
                                                            <label class="input-group-text">Quarter</label>
                                                        </div>
                                                        <select class="custom-select" v-model="quarter" name="quarter">
                                                            <option v-for="i in [1,2,3,4]">{{i}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <button type="submit" class="box__button">Upload</button>
                                            </div>
                                        </div>
                                        <div class="box__uploading"><img src="<?php echo STYLE_HOST; ?>/img/load.gif">
                                        </div>
                                        <div class="box__success">เพิ่มข้อมูลเสร็จสิ้น ! </div>
                                        <div class="box__error">Error! <span></span>. <a href="?submit-on-demand"
                                                class="box__restart" role="button">Try
                                                again!</a></div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="email" role="tabpanel" aria-labelledby="email-tab">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Email Sender Setting</h4>

                                        <div class="form-group row">
                                            <label for="inputHorizontalSuccess"
                                                class="col-sm-2 col-form-label text-dark">Email Sender</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="inputHorizontalSuccess"
                                                    placeholder="name@example.com" v-model="data.email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="nameSender" class="col-sm-2 col-form-label text-dark">Name
                                                Sender
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="nameSender"
                                                    v-model="data.nameSender">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="subject" class="col-sm-2 col-form-label text-dark">Subject
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="subject"
                                                    v-model="data.subject">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="msgToDept" class="col-sm-2 col-form-label text-dark">Message
                                                To
                                                Dept</label>
                                            <div class="col-sm-10">
                                                <div class="summernote" id="msgToDept"></div>
                                                <button @click="testEmail('dept')" class="btn btn-info  font-bold "><i
                                                        class="mdi mdi-email-send font-20"></i> TEST SEND
                                                    E-MAIL</button>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="msgDeptApprove"
                                                class="col-sm-2 col-form-label text-dark">Message
                                                To
                                                Manager When Approve</label>
                                            <div class="col-sm-10">
                                                <div class="summernote" id="msgDeptApprove"></div>
                                                <button @click="testEmail('approve')"
                                                    class="btn btn-info  font-bold "><i
                                                        class="mdi mdi-email-send font-20"></i> TEST SEND
                                                    E-MAIL</button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="msgDeptReject" class="col-sm-2 col-form-label text-dark">Message
                                                To
                                                Manager When Reject</label>
                                            <div class="col-sm-10">
                                                <div class="summernote" id="msgDeptReject"></div>
                                                <button @click="testEmail('reject')" class="btn btn-info  font-bold "><i
                                                        class="mdi mdi-email-send font-20"></i> TEST SEND
                                                    E-MAIL</button>
                                            </div>
                                        </div>
                                        <button @click="saveEmail()" class="btn btn-success  font-bold "><i
                                                class="mdi mdi-content-save font-20"></i> S A V
                                            E</button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="delete" role="tabpanel" aria-labelledby="settings-tab">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Delete Data</h4>
                                        <div class="form-group mb-4">
                                            <label class="mr-sm-2 col-4 text-right" for="year">Select year</label>
                                            <select class="custom-select mr-sm-2 col-7" id="year" v-model="delYear">
                                                <option value="0" selected="">Choose...</option>
                                                <option v-for="(item,index) in data.year" :value="index">{{index}}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label class="mr-sm-2 col-4 text-right">Select Quarter</label>
                                            <select class="custom-select mr-sm-2 col-7" v-model="delQuarter">
                                                <option value="0" selected="">Choose...</option>
                                                <option v-for="i in listQuarter">{{i}}</option>
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <button type="button" @click="deleteData()"
                                                :disabled="delYear==0 || delQuarter==0"
                                                class="btn btn-rounded waves-effect waves-light btn-block btn-danger"><i
                                                    class="mdi mdi-trash-can-outline"></i>&nbsp Delete
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    let vue = new Vue({
        el: '#app',
        data: {
            key: 0,
            data: {
                message: ""
            },
            delYear: 0,
            listQuarter: [],
            delQuarter: 0,
            yearSelect: year,
            optionYear: [year - 1, year, year + 1],
            quarter: Math.ceil(month / 3),
            totalScore: 0,
        },
        created() {
            this.key = Math.floor((Math.random(10, 999) * 100) + 1)
            $.ajax({
                    method: "GET",
                    url: "",
                    data: {
                        getData: 1
                    },
                    dataType: 'json'
                })
                .then(response => {
                    if (response.error) {
                        Swal.fire({
                            position: 'top-center',
                            type: 'error',
                            title: `Error`,
                            text: response.error,
                            showConfirmButton: true
                        })
                    } else {
                        this.data = response
                        $('.summernote').summernote({
                            height: 150, //set editable area's height
                            codemirror: { // codemirror options
                                theme: 'monokai'
                            }
                        });
                        $('#msgToDept').summernote('code', decodeURIComponent(response.msgToDept))
                        $('#msgDeptApprove').summernote('code', decodeURIComponent(response
                            .msgDeptApprove))
                        $('#msgDeptReject').summernote('code', decodeURIComponent(response
                            .msgDeptReject))
                    }
                })
                .catch(error => {
                    console.log(error)
                });

        },
        methods: {
            saveEmail() {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        saveEmail: 1,
                        data: this.data,
                        msgToDept: encodeURIComponent($('#msgToDept').summernote('code')),
                        msgDeptApprove: encodeURIComponent($('#msgDeptApprove').summernote(
                            'code')),
                        msgDeptReject: encodeURIComponent($('#msgDeptReject').summernote(
                            'code')),
                    },
                }).then(response => {
                    if (response.success === true) {
                        Swal.fire({
                            position: 'top-center',
                            type: 'success',
                            title: `save data is success`,
                            showConfirmButton: true,
                            timer: 2000
                        })
                        this.emp.data.status = 2
                        this.emp.data.emp_comment = $('#emp_comment')
                            .summernote('code')
                    } else {
                        Swal.fire({
                            position: 'top-center',
                            type: 'error',
                            title: `Error`,
                            text: response.error,
                            showConfirmButton: true
                        })
                    }
                }).catch(error => {
                    toastr.error(`Error save EN-${id}`, 'ERROR');
                })
            },
            testEmail(type) {
                Swal.fire({
                    title: 'Please Input E-mail',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off',
                        placeholder: 'test@test.com'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Send Email',
                    showLoaderOnConfirm: true,
                    preConfirm: (email) => {
                        return $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                type: type,
                                testEmail: email
                            },
                        }).then(response => {
                            if (response.success === true) {
                                Swal.fire({
                                    title: 'All done!',
                                    confirmButtonText: 'Okey!'
                                })
                            } else {
                                Swal.showValidationMessage(
                                    `Request failed: ${response.error}`
                                )
                            }
                        }).catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                    },
                })
            },
            deleteData() {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off',
                        placeholder: `Plese input key ${this.key} for validate! `
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                    preConfirm: (key) => {
                        if (key != this.key) {
                            swalWithBootstrapButtons.showValidationMessage(
                                `Key Not Match`
                            )
                        }
                        return $.ajax({
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    deleteYear: true,
                                    year: this.delYear,
                                    quarter: this.delQuarter
                                }
                            }).then(response => {
                                // if (!response.success) {
                                //     throw new Error(response.statusText)
                                // }
                                return response
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error.value}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value.success) {
                        swalWithBootstrapButtons.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    } else if (result.value.error) {
                        swalWithBootstrapButtons.fire(
                            'Error!',
                            'Please Contract Webmaster .',
                            'error'
                        )
                    }
                }).catch(error => {

                })
            }
        },
        computed: {},
        watch: {
            delYear(cur, old) {
                this.listQuarter = this.data.year[cur]
            }
            // years() {
            //     console.log(this.years)
            // }
        }
    })
    var isAdvancedUpload = function() {
        var div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in
            window && 'FileReader' in window;
    }();

    var forms = document.querySelectorAll('.box');
    Array.prototype.forEach.call(forms, function(form) {
        var input = form.querySelector('input[type="file"]'),
            label = form.querySelector('label'),
            errorMsg = form.querySelector('.box__error span'),
            restart = form.querySelectorAll('.box__restart'),
            droppedFiles = false,
            showFiles = function(files) {
                $("#upLoad").show()
                label.textContent = files.length > 1 ? (input.getAttribute('data-multiple-caption') ||
                    '').replace('{count}', files.length) : files[0].name;
            },
            triggerFormSubmit = function() {
                var event = document.createEvent('HTMLEvents');
                event.initEvent('submit', true, false);
                form.dispatchEvent(event);
            };

        var ajaxFlag = document.createElement('input');
        ajaxFlag.setAttribute('type', 'hidden');
        ajaxFlag.setAttribute('name', 'ajax');
        ajaxFlag.setAttribute('value', 1);
        form.appendChild(ajaxFlag);

        $('input[type="file"]').on('change', function(e) {
            showFiles(e.target.files)

        });

        if (isAdvancedUpload) {
            form.classList.add(
                'has-advanced-upload'
            ); // letting the CSS part to know drag&drop is supported by the browser

            ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(
                function(event) {
                    form.addEventListener(event, function(e) {
                        // preventing the unwanted behaviours
                        e.preventDefault();
                        e.stopPropagation();
                    });
                });
            ['dragover', 'dragenter'].forEach(function(event) {
                form.addEventListener(event, function() {
                    form.classList.add('is-dragover');
                });
            });
            ['dragleave', 'dragend', 'drop'].forEach(function(event) {
                form.addEventListener(event, function() {
                    form.classList.remove('is-dragover');
                });
            });
            form.addEventListener('drop', function(e) {
                droppedFiles = e.dataTransfer.files; // the files that were dropped
                showFiles(droppedFiles);

            });
        }


        // if the form was submitted
        form.addEventListener('submit', function(e) {
            // preventing the duplicate submissions if the current one is in progress
            if (form.classList.contains('is-uploading')) return false;

            form.classList.add('is-uploading');
            form.classList.remove('is-error');

            if (isAdvancedUpload) // ajax file upload for modern browsers
            {
                e.preventDefault();

                // gathering the form data
                var ajaxData = new FormData(form);
                if (droppedFiles) {
                    Array.prototype.forEach.call(droppedFiles, function(file) {
                        ajaxData.append(input.getAttribute('name'), file);
                    });
                }

                // ajax request
                var ajax = new XMLHttpRequest();
                ajax.open(form.getAttribute('method'), form.getAttribute('action'), true);

                ajax.onload = function() {
                    form.classList.remove('is-uploading');
                    if (ajax.status >= 200 && ajax.status < 400) {
                        var data = JSON.parse(ajax.responseText);
                        form.classList.add(data.success == true ? 'is-success' :
                            'is-error');
                        $.get
                        if (!data.success) errorMsg.textContent = data.error;
                    } else alert('Error. Please, contact the webmaster!');
                };

                ajax.onerror = function() {
                    form.classList.remove('is-uploading');
                    alert('Error. Please, try again!');
                };

                ajax.send(ajaxData);
            } else // fallback Ajax solution upload for older browsers
            {
                var iframeName = 'uploadiframe' + new Date().getTime(),
                    iframe = document.createElement('iframe');

                $iframe = $('<iframe name="' + iframeName +
                    '" style="display: none;"></iframe>');

                iframe.setAttribute('name', iframeName);
                iframe.style.display = 'none';

                document.body.appendChild(iframe);
                form.setAttribute('target', iframeName);

                iframe.addEventListener('load', function() {
                    var data = JSON.parse(iframe.contentDocument.body.innerHTML);
                    form.classList.remove('is-uploading')
                    form.classList.add(data.success == true ? 'is-success' : 'is-error')
                    form.removeAttribute('target');
                    if (!data.success) errorMsg.textContent = data.error;
                    iframe.parentNode.removeChild(iframe);
                });
            }
        });


        // restart the form if has a state of error/success
        Array.prototype.forEach.call(restart, function(entry) {
            entry.addEventListener('click', function(e) {
                e.preventDefault();
                form.classList.remove('is-error', 'is-success');
                input.click();
            });
        });

        // Firefox focus bug fix for file input
        input.addEventListener('focus', function() {
            input.classList.add('has-focus');
        });
        input.addEventListener('blur', function() {
            input.classList.remove('has-focus');
        });
    });
})
</script>