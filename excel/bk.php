<?php
date_default_timezone_set("Asia/Bangkok");
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!$data = $_SESSION['eva']['excel']) {
    echo "No Data";
    exit();
}
// echo "<pre>";print_r( $data);exit();

set_time_limit(200);
require_once "class.writeexcel_workbookbig.inc.php";
require_once "class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "merge2.xls");
$y = array(
    's' => date("y") - 1,
    'e' => date("y"),
    'S' => date("Y") - 1,
    'E' => date("Y"),
    'ss' => date("Y") + 542,
    'ee' => date("Y") + 543,
);
$workbook = new writeexcel_workbookbig($fname);
$worksheet = &$workbook->addworksheet();
// $worksheet->set_margin_right(0.30);
// $worksheet->set_margin_bottom(1.4);
// $worksheet->hide_gridlines();
// $worksheet->set_margin_left(0.80);
$textheader = array(
    'Attendance การมาทำงานและการตรงต่อเวลา : พิจารณาถึง การมาปฏิบัติงานและการตรงต่อเวลา',
    'Quantityปริมาณงาน : พิจารณาถึงปริมาณงานที่สามารถทำได้ตามเป้าหมายที่กำหนดหรือที่ได้รับมอบหมาย',
    'Quality คุณภาพงาน : พิจารณาถึงคุณภาพงานที่ส่งมอบกับมาตราฐานที่กำหนด',
    'Accountability & Delivery ความรับผิดชอบและความรวดเร็วในการส่งมอบงาน พิจารณาถึงความรับผิดชอบในการทำงานที่ได้รับมอบหมายให้เสร็จลุล่วงด้วยตนเองและความรวดเร็วในการส่งมอบงานในระยะเวลาที่กำหนด',
    'Job Knowledge and skill ความรู้/ทักษะในการทำงาน : พิจารณาถึงความรู้ทักษะ เทคนิค วิธีการ ความชำนาญ หรือมีทักษะมากกว่า 1 ทักษะ ที่นำมาใช้เพื่อให้งานสำเร็จลุล่วง',
    'Work Discipline การปฎิบัติตามกฎระเบียบ :  พิจาณาถึงการปฎิบัติตามกฎระเบียบของพื้นที่การทำงาน',
    'Teamwork/Cooperation & Company Contribution การทำงานเป็นทีมและการให้ความร่วมมือกับองค์กร : พิจารณาถึงความสัมพันธ์ในการทำงานกับพนักงานคนอื่น และการให้ความร่วมมือกับหน่วยงานและหัวหน้างานในการทำงานที่ได้รับหมอบหมาย',
    'Safety and health ความปลอดภัยและสุขอนามัย : พิจารณาถึงการกระทำที่ปลอดภัยและถูกสุขอนามัยต่อตนเอง'
);
$fullScore = array(10, 20, 20, 15, 10, 10, 10, 5);
$worksheet->set_row('0', 32);
$worksheet->set_row('1', 223);
$worksheet->set_row('3', 15);
$worksheet->set_row('4', 38);
$worksheet->set_zoom(90);
$worksheet->hide_gridlines(2);

$worksheet->set_column('A:A', 7);
$worksheet->set_column('B:B', 9);
$worksheet->set_column('C:C', 27);
$worksheet->set_column('D:D', 8);
$worksheet->set_column('E:E', 27);
$worksheet->set_column('F:J', 13);
$worksheet->set_column('K:L', 9);
$worksheet->set_column('M:O', 23);

$center  = &$workbook->addformat(array('align' => 'center'));

$ferrari = &$workbook->set_custom_color(40, 214, 220, 228);
$gray = &$workbook->set_custom_color(41, 191, 191, 191);
$green = &$workbook->set_custom_color(42, 204, 255, 204);
$sky = &$workbook->set_custom_color(43, 204, 255, 255);
$orange2 = &$workbook->set_custom_color(44, 252, 228, 214);
$yellow2 = &$workbook->set_custom_color(45, 255, 255, 204);

$header = array(
    'font' => 'Tahoma',
    'size'    => 10,
    'bold' => true,
    'align' => 'vcenter',
    'left' => 1,
    'right' => 1,
    'text_wrap' => 1
);
$header_nobg = &$workbook->addformat($header);
$header_nobg->set_align('center');

$header_nobg_top = &$workbook->addformat($header);
$header_nobg_top->set_align('top');
$header_nobg_top->set_align('center');

$header_nobg_bottom = &$workbook->addformat($header);
$header_nobg_bottom->set_align('center');
$header_nobg_bottom->set_bottom(1);

$header_bg1 = &$workbook->addformat($header);
$header_bg1->set_bg_color($ferrari);

$header_bg1_center = &$workbook->addformat($header);
$header_bg1_center->set_bg_color($ferrari);
$header_bg1_center->set_align('center');

$header_bg2 = &$workbook->addformat($header);
$header_bg2->set_bg_color($gray);
$header_bg2->set_align('center');


$header_bg3 = &$workbook->addformat($header);
$header_bg3->set_bg_color('yellow');
$header_bg3->set_align('bottom');
$header_bg3->set_align('center');
$header_bg3->set_size('12');

$header_bg4 = &$workbook->addformat($header);
$header_bg4->set_top();
$header_bg4->set_bg_color($green);
$header_bg4->set_align('center');

$header_bg5 = &$workbook->addformat($header);
$header_bg5->set_align('bottom');
$header_bg5->set_align('center');

$header_bg5_top = &$workbook->addformat($header);
$header_bg5_top->set_align('top');
$header_bg5_top->set_top();
$header_bg5_top->set_align('center');

$header_bg6 = &$workbook->addformat($header);
$header_bg6->set_bg_color($sky);
$header_bg6->set_top();
$header_bg6->set_align('center');

$header_bg7 = &$workbook->addformat($header);
$header_bg7->set_bg_color($orange2);;
$header_bg7->set_align('center');
// ---------------------- BODY ------------------//
$body = array(
    'font' => 'Cordia New',
    'size'    => 14,
    'align' => 'center',
    'left' => 1,
    'top' => 1
);


$body_nobg = &$workbook->addformat($body);

$body_nobg_wrap = &$workbook->addformat($body);
$body_nobg_wrap->set_text_wrap();

$body_name = &$workbook->addformat($body);
$body_name->set_align('left');

$body_left = &$workbook->addformat($body);
$body_left->set_align('left');
$body_left->set_right(1);
$body_left->set_text_wrap();

$body_score = &$workbook->addformat($body);
$body_score->set_bg_color($orange2);

$body_blue = &$workbook->addformat($body);
$body_blue->set_color('blue');

$body_bg_sky = &$workbook->addformat($body);
$body_bg_sky->set_bg_color($sky);

$body_bg_yellow = &$workbook->addformat($body);
$body_bg_yellow->set_bg_color($yellow2);
$body_bg_yellow->set_color('blue');

for ($row = 0; $row <= 4; $row++) {
    for ($col = 1; $col <= 46; $col++) {
        if ($col > 41) {
            if ($row > 0) $worksheet->write($row, $col, '', $header_bg5_top);
        } else  if ($col > 34) {
            $worksheet->write($row, $col, '', $header_nobg_bottom);
        } else {
            $worksheet->write($row, $col, '', $header_nobg);
        }
    }
}

$worksheet->write('A2', 'NO', $header_nobg);
$worksheet->write('B2', 'EN', $header_nobg);
$worksheet->write('C2', 'NAME', $header_nobg);
$worksheet->write('D2', 'JOB TITLE', $header_nobg);
$column = 4;
foreach ($textheader as $k => $v) {
    $worksheet->write(0, $column, "(" . ($k + 1) . ")", $header_bg1_center);
    $worksheet->write(1, $column,   iconv('UTF-8', 'cp874', $v), $header_bg1);
    $worksheet->write(2, $column,  'Full Score ' . $fullScore[$k], $header_nobg);
    $worksheet->write(3, $column,   iconv('UTF-8', 'cp874', 'คะแนนเต็ม ' . $fullScore[$k]), $header_bg1);
    $worksheet->merge_cells(3, $column, 4, $column);
    $column++;
}

$other = array(
    'TOTAL SCORE คะแนนรวม',
    'PERF. RATING ระดับผลการปฏิบัติงาน',
    'WAGE INCREASE (จำนวนเงินขึ้น)',
    'PRORATE DAY จำนวนวันที่ได้รับการประเมินผล',
    'WAGE PER DAY  ค่าจ้างต่อวัน',
    'ACTUAL WAGE INCREASE จำนวนเงินที่ได้ขี้นจริง',
    'NEW WAGE PER DAY ค่าจ้างใหม่ที่ได้รับต่อวัน'
);
foreach ($other as $k => $v) {
    if ($column > 13) {
        $style = $header_nobg;
    } else {
        $style = $header_bg2;
    }
    $worksheet->write(0, $column, iconv('UTF-8', 'cp874', $v), $style);
    $worksheet->merge_cells(0, $column, 3, $column);
    $worksheet->write_blank(4, $column, $style);
    $column++;
}

$promo = array('PROMOTION', 'SPECIAL ADJUSTMENT', 'FINAL NEW WAGE');
foreach ($promo as $k => $v) {
    $worksheet->write(0, $column, iconv('UTF-8', 'cp874', $v), $header_bg3);
    $worksheet->merge_cells(0, $column, 4, $column);
    $column++;
}

$worksheet->write('W1', iconv('UTF-8', 'cp874', "LEAVE RECORDS AVIALABLE FROM FEB 1' " . $y['s'] . " - JAN 31' " . $y['e'] . "  
(ข้อมูลการลางานระหว่างวันที่ 1 กุมภาพันธ์ " . $y['ss'] . " - 31 มกราคม " . $y['ee'] . ")"), $header_bg4);
$worksheet->merge_cells('W1:AH2');

$worksheet->write('AI2', iconv('UTF-8', 'cp874', "Disciplinary Action Item available in ER Database from February 1, " . $y['S'] . " - January 31, " . $y['E'] . "  
(ข้อมูลที่ปรากฏได้มาจากระบบข้อมูลของ Disciplinary Action ของ ER ระหว่างวันที่ 1 กุมภาพันธ์ " . $y['ss'] . " - 31 มกราคม " . $y['ee'] . ")"), $header_bg4);
$worksheet->merge_cells('AI2:AL2');

$worksheet->write('AM2', iconv('UTF-8', 'cp874', 'SUPERVISOR COMMENT (ความคิดเห็นของหัวหน้างาน)'), $header_bg6);
$worksheet->merge_cells('AM2:AP2');

$worksheet->write('AI1', iconv('UTF-8', 'cp874', 'VARIOUS INFORMATION FOR BEING ONE SOURCE OF REFERENCE AND CONSIDERATION ONLY.  PLEASE TAKE YOUR APPROPRIATE JUDGEMENT FOR USING THE DATA. (ข้อมูลต่าง ๆ ที่นำมาแสดงให้เหล่านี้ เป็นเพียงทางเลือกหนึ่ง ที่นำมาใช้ประกอบการพิจารณาตามความเหมาะสม กรุณาใช้วิจารณญาณ'), $header_bg7);
$worksheet->merge_cells('AI1:AP1');


$leave = array('CL', 'SL', 'MTL', 'MRL', 'PTL', 'PRL', 'UL', 'ABS', 'Leave', 'Late', 'Late', 'Late', 'VW (เตือนด้วยวาจา)', 'WL1 (เตือนเป็นลายลักษณ์อักษรครั้งที่1)', 'WL2 (เตือนเป็นลายลักษณ์อักษรครั้งที่สุดท้าย)', 'จำนวนที่ถูกพักงาน', 'จุดเด่นของผู้ถูกประเมิน', 'จุดที่ต้องพัฒนา', 'เป้าหมายที่คาดหวังปีต่อไป', 'แผนการฝึกอบรม', 'BU', 'DEPARTMENT', 'SUPV-NAME', 'MGR.', 'GM');
$leave2 = array('(ลากิจ)', '(ลาป่วย)', '(ลาคลอด)', '(ลาแต่ง)', '(ลาบิดา)', '(ลาบวช)', '(ลาไม่ได้ค่าจ้าง)', '(ขาดงาน)', 'Total (Days)', '(Times)', '(Hours)', '(Minutes)');
$column = 22;
foreach ($leave as $k => $v) {
    if ($column > 41) {
        $worksheet->write(2, $column, iconv('UTF-8', 'cp874', $v), $header_nobg_top);
        $worksheet->merge_cells(2, $column, 4, $column);
    } else if ($column > 33) {
        $worksheet->write(2, $column, iconv('UTF-8', 'cp874', $v), $header_bg5_top);
        $worksheet->merge_cells(2, $column, 4, $column);
    } else {
        $worksheet->write(2, $column, iconv('UTF-8', 'cp874', $v), $header_bg5_top);
        $worksheet->write(3, $column, iconv('UTF-8', 'cp874', $leave2[$k]), $header_bg5);
        $worksheet->merge_cells(3, $column, 4, $column);
    }
    $column++;
}


$row = 5;
foreach ($data as $k => $v) {
    $worksheet->set_row($row, 21);
    $col = 0;
    $m = "M" . $row_c;
    $row_c = $row + 1;
    $worksheet->write($row, $col++, $k + 1, $body_nobg);
    $worksheet->write_string($row, $col++, $v['en'], $body_nobg);
    $worksheet->write($row, $col++, $v['name'] . ' ' . $v['surname'], $body_name);
    $worksheet->write($row, $col++, $v['position'], $body_name);
    for ($i = 1; $i <= 8; $i++) {
        if ($i == 1) {
            $worksheet->write($row, $col++, $v['score' . $i], $body_score);
        } else {
            $worksheet->write($row, $col++, $v['score' . $i], $body_nobg);
        }
    }
    $worksheet->write_formula($row, $col++, "=SUM(E" . $row_c . ":L" . $row_c . ")", $body_nobg);
    // $worksheet->write_formula($row, $col++, "=IF(M" . $row_c . "<=50,1,IF(M" . $row_c . "<=69,2,IF(M" . $row_c . "<=85,3,IF(M" . $row_c . "<=95,4,5))))", $body_nobg);
    $worksheet->write($row, $col++, $v['grade'], $body_nobg);
    // $worksheet->write($row, $col++,"=IF(M".$row_c.">95,14,IF(M".$row_c.">90,12,IF(M".$row_c.">85,11,IF(M".$row_c.">83,10,IF(M".$row_c.">80,9,IF(M".$row_c.">78,8,IF(M".$row_c.">75,7,IF(M".$row_c.">72,".$row_c.",IF(M".$row_c.">".$row_c."9,5,IF(M".$row_c.">".$row_c."4,4,IF(M".$row_c.">".$row_c."0,3,IF(M".$row_c.">55,2,IF(M".$row_c.">50,1,0)))))))))))))", $body_nobg);
    $worksheet->write($row, $col++, $v['moneyup'], $body_bg_sky);
    $worksheet->write($row, $col++, $v['20_mar'], $body_nobg);
    $worksheet->write($row, $col++, $v['sum_w'], $body_nobg);
    $worksheet->write_formula($row, $col++, "=ROUNDUP((O" . $row_c . "*P" . $row_c . "/365),0)", $body_blue);
    $worksheet->write_formula($row, $col++, "=SUM(Q" . $row_c . ":R" . $row_c . ")", $body_blue);
    $worksheet->write($row, $col++, $v['promotion'], $body_bg_yellow);
    $worksheet->write($row, $col++, $v['special'], $body_bg_yellow);
    $worksheet->write_formula($row, $col++, "=S" . $row_c . "+T" . $row_c . "+U" . $row_c, $body_bg_yellow);
    $worksheet->write($row, $col++, $v['cl'], $body_nobg);
    $worksheet->write($row, $col++, $v['sl'], $body_nobg);
    $worksheet->write($row, $col++, $v['mtl'], $body_nobg);
    $worksheet->write($row, $col++, $v['mrl'], $body_nobg);
    $worksheet->write($row, $col++, $v['ptl'], $body_nobg);
    $worksheet->write($row, $col++, $v['prl'], $body_nobg);
    $worksheet->write($row, $col++, $v['ul'], $body_nobg);
    $worksheet->write($row, $col++, $v['abs'], $body_nobg);
    $worksheet->write($row, $col++, $v['sum_l'], $body_nobg);
    $worksheet->write($row, $col++, $v['late_time'], $body_nobg);
    $worksheet->write($row, $col++, $v['late_hour'], $body_nobg);
    $worksheet->write($row, $col++, $v['late_min'], $body_nobg);
    $worksheet->write($row, $col++, $v['vw'], $body_nobg_wrap);
    $worksheet->write($row, $col++, $v['wl1'], $body_nobg_wrap);
    $worksheet->write($row, $col++, $v['wl2'], $body_nobg_wrap);
    $worksheet->write($row, $col++, $v['without_pay'], $body_nobg_wrap);
    $worksheet->write($row, $col++, iconv('UTF-8', 'cp874', $v['comment1']), $body_left);
    $worksheet->write($row, $col++, iconv('UTF-8', 'cp874', $v['comment2']), $body_left);
    $worksheet->write($row, $col++, iconv('UTF-8', 'cp874', $v['comment3']), $body_left);
    $worksheet->write($row, $col++, iconv('UTF-8', 'cp874', $v['comment4']), $body_left);
    $worksheet->write($row, $col++, $v['bu'], $body_left);
    $worksheet->write($row, $col++, $v['dept'], $body_left);
    $worksheet->write($row, $col++, $v['supervisor'], $body_left);
    $worksheet->write($row, $col++, $v['manager'], $body_left);
    $worksheet->write($row, $col++, $v['gm'], $body_left);
    $row++;
}
// ---------------------END BODY ------------------//







$workbook->close();

header("Content-Type: application/x-msexcel; name=\"return.xls\"");
header("Content-Disposition: inline; filename=\"return.xls\"");
$fh = fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

function write_blank($worksheet,  $style)
{

    for ($row = 0; $row <= 4; $row++) {
        for ($col = 1; $col <= 45; $col++) {
            $worksheet->write($row, $col, '', $style);
        }
    }
}
