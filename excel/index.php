<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!$data = $_SESSION['eva']['excel']) {
    echo "No Data";
    exit();
}
// echo "<pre>";
// print_r($data);
// exit();

set_time_limit(200);
require_once "class.writeexcel_workbookbig.inc.php";
require_once "class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "merge2.xls");
$workbook = new writeexcel_workbookbig($fname);
$worksheet = &$workbook->addworksheet();

$worksheet->set_row('2', 27);
$worksheet->set_row('3', 30);
$worksheet->set_row('4', 30);
$worksheet->set_zoom(100);
$worksheet->hide_gridlines(2);

$worksheet->set_column('A:A', 7);
$worksheet->set_column('B:B', 9);
$worksheet->set_column('C:C', 27);
$worksheet->set_column('D:D', 8);
$worksheet->set_column('E:E', 27);
$worksheet->set_column('F:J', 15);
$worksheet->set_column('K:K', 9);
$worksheet->set_column('L:L', 13);
$worksheet->set_column('M:O', 30);

$center = &$workbook->addformat(array('align' => 'center'));

$pink = &$workbook->set_custom_color(40, 255, 204, 204);
$blue = &$workbook->set_custom_color(41, 102, 255, 255);
$yellow = &$workbook->set_custom_color(42, 255, 255, 153);
$darkPink = &$workbook->set_custom_color(43, 255, 204, 255);
$darkBlue = &$workbook->set_custom_color(44, 0, 255, 255);
$liteYellow = &$workbook->set_custom_color(45, 255, 255, 204);

$header = array(
    'font' => 'Tahoma',
    'size' => 10,
    'bold' => true,
    'left' => 1,
    'right' => 1,
    'text_wrap' => 1,
);

$header_pink = &$workbook->addformat($header);
$header_pink->set_align('center');
$header_pink->set_bg_color($pink);

$header_blue = &$workbook->addformat($header);
$header_blue->set_align('center');
$header_blue->set_align('vcenter');
$header_blue->set_top(1);
$header_blue->set_bg_color($blue);

$header_blue_bottom = &$workbook->addformat($header);
$header_blue_bottom->set_bg_color($blue);
$header_blue_bottom->set_color('blue');
$header_blue_bottom->set_align('center');

$header_back_bottom = &$workbook->addformat($header);
$header_back_bottom->set_bg_color($blue);
$header_back_bottom->set_color('back');
$header_back_bottom->set_align('center');

$head = array("Organization Objectives", 'Quality By Objectives', 'Continuous Improvement', 'Attendance   Records', 'Personal Development ');
$worksheet->write('E1', 'ITEM', $header_pink);
$worksheet->write('E2', 'GOAL / KPI', $header_pink);
$worksheet->write('E3', '', $header_pink);
$worksheet->merge_cells('E2:E3');
$column = 5;
foreach ($head as $k => $v) {
    $worksheet->write(0, $column, "(" . ($k) . ")", $header_pink);
    $worksheet->write(1, $column, $v, $header_pink);
    $worksheet->write(2, $column, '', $header_pink);
    $worksheet->merge_cells(1, $column, 2, $column);
    $column++;
}
$column = 0;
$head = array("NO.", "EN", "NAME", "LEVEL", "POSITION", "SCORE", "SCORE", "SCORE", "SCORE", "SCORE", " TOTAL", "Performance", "BU", "MGR.", "BU.HEAD");
$head2 = array(25, 25, 20, 20, 10, "SCORE", "Rating");
foreach ($head as $k => $v) {
    $worksheet->write(3, $column, $v, $header_blue);
    if ($k >= 5 and $k <= 11) {
        if ($k > 9) {
            $worksheet->write(4, $column, $head2[$k - 5], $header_back_bottom);
        } else {
            $worksheet->write(4, $column, $head2[$k - 5], $header_blue_bottom);
        }
    } else {
        $worksheet->write(4, $column, '', $header_blue);
        $worksheet->merge_cells(3, $column, 4, $column);
    }
    $column++;
}

$bodyStyle = array(
    'font' => 'Tahoma',
    'size' => 10,
    'left' => 1,
    'top' => 1,
    'bottom' => 1,
    'right' => 1,
    'text_wrap' => 1,
);

$body = &$workbook->addformat($bodyStyle);

$body_center = &$workbook->addformat($bodyStyle);
$body_center->set_align('center');

$body_yellow = &$workbook->addformat($bodyStyle);
$body_yellow->set_align('center');
$body_yellow->set_bg_color($yellow);
$body_yellow->set_bold(true);

$body_lite_yellow = &$workbook->addformat($bodyStyle);
$body_lite_yellow->set_align('center');
$body_lite_yellow->set_bg_color($liteYellow);
$body_lite_yellow->set_bold(true);

$body_pink = &$workbook->addformat($bodyStyle);
$body_pink->set_align('center');
$body_pink->set_bg_color($darkPink);
$body_pink->set_bold(true);

$body_blue = &$workbook->addformat($bodyStyle);
$body_blue->set_align('center');
$body_blue->set_bg_color($darkBlue);
$body_blue->set_bold(true);

$row = 5;
foreach ($data['data'] as $k => $v) {
    $worksheet->set_row($row, 19);
    $col = 0;
    $worksheet->write($row, $col++, $k + 1, $body_center);
    $worksheet->write($row, $col++, $v['en'] . " ", $body_center);
    $worksheet->write($row, $col++, $v['name'], $body);
    $worksheet->write($row, $col++, $v['level'], $body_center);
    $worksheet->write($row, $col++, $v['position'], $body);
    $worksheet->write($row, $col++, $v['score1'], $body_yellow);
    $worksheet->write($row, $col++, $v['score2'], $body_pink);
    $worksheet->write($row, $col++, $v['score3'], $body_yellow);
    if ($v['lv'] == "exem") {
        $worksheet->write($row, $col++, $v['score4'], $body_pink);
    } else {
        $worksheet->write($row, $col++, $v['score4'], $body_yellow);
    }
    $worksheet->write($row, $col++, $v['score5'], $body_yellow);
    $worksheet->write($row, $col++, $v['total'], $body_blue);
    $worksheet->write($row, $col++, $v['grade'], $body_lite_yellow);
    $worksheet->write($row, $col++, $v['bu'], $body);
    $worksheet->write($row, $col++, $v['manager'], $body);
    $worksheet->write($row, $col++, $v['dept'], $body);
    $row++;
}

$workbook->close();
$fileName = "Return_PB_Q" . $data['quarter'] . "FY" . $data['year'] . ".xls";
// echo "Dsa";
// echo $fileName;exit();
header("Content-Type: application/x-msexcel; name=" . $fileName);
header("Content-Disposition: inline; filename=" . $fileName);
$fh = fopen($fname, "rb");
fpassthru($fh);
unlink($fname);