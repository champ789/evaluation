<?php
error_reporting(-1); // reports all errors
ini_set("display_errors", "1"); // shows all errors
ini_set("log_errors", 1);
define('SITE', 'evaluation');
define('DB_PREFIX', 'db');
date_default_timezone_set("Asia/Bangkok");
set_time_limit(60);

define('ROOT_PATH', dirname(__FILE__) . '/');
define('CORE_PATH', dirname(__FILE__) . '/__core/');
define('HTTP_HOST', $_SERVER['HTTP_HOST']);
define('LOCAL_HOST', 'http://' . HTTP_HOST . '/PB_online/');
define('STYLE_HOST', LOCAL_HOST . '__assets/');
define('APP_NAME', 'PB');
include('CAS/CAS.php');
ob_start("ob_gzhandler");
// session_start();
require CORE_PATH . 'registry.php';
require CORE_PATH . 'controller.php';

$_display = 'desktop';
$_template = $_display;

define('TEMPLATE', $_template);

if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}

$controller = new Controller();

$action = 'home';
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

$method = 'index';
if (isset($_GET['method'])) {
    $method = $_GET['method'];
}

$controller->action($action, array(), $method);

unset($controller);
exit();
